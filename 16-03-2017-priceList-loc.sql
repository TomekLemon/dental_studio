-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               10.1.21-MariaDB - mariadb.org binary distribution
-- Serwer OS:                    Win32
-- HeidiSQL Wersja:              9.4.0.5137
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela dentalstudio.price_list
CREATE TABLE IF NOT EXISTS `price_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Zrzucanie danych dla tabeli dentalstudio.price_list: ~24 rows (około)
/*!40000 ALTER TABLE `price_list` DISABLE KEYS */;
INSERT INTO `price_list` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
	(3, 'Usuwanie kamienia nazębnego i pod dziąsłowego', '100,00 zł', '2017-03-15 14:53:58', '2017-03-16 11:53:51'),
	(4, 'Piaskowanie', '100,00 zł', '2017-03-15 15:29:20', '2017-03-16 11:54:07'),
	(5, 'Znieczulenie', 'Od 30 zł', '2017-03-16 11:54:21', NULL),
	(6, 'Znieczulenie Komputerowe', '50,00 zł', '2017-03-16 11:54:29', NULL),
	(7, 'Sedacja wziewna Podtlenkiem Azotu', '100,00 zł', '2017-03-16 11:54:36', NULL),
	(8, 'Wypełnienie w zębie mlecznym', '100,00 zł', '2017-03-16 11:54:44', NULL),
	(9, 'Wypełnienie w zębie stałym', 'Od 130 zł', '2017-03-16 11:54:53', NULL),
	(10, 'Leczenie kanałowe pierwotne z użyciem mikroskopu', 'Od 300 zł', '2017-03-16 11:55:00', NULL),
	(11, 'Leczenie kanałowe powtórne z użyciem mikroskopu', 'Od 400 zł', '2017-03-16 11:55:08', NULL),
	(12, 'Lakowanie', '50,00 zł', '2017-03-16 11:55:18', NULL),
	(13, 'Lakierowanie', '100,00 zł', '2017-03-16 11:55:25', NULL),
	(14, 'Ozonowanie 1 zęba', '70,00 zł', '2017-03-16 11:55:35', NULL),
	(15, 'Wkład koronowo - korzeniowy lany', '350,00 zł - 400 zł', '2017-03-16 11:55:46', NULL),
	(16, 'Korona porcelanowa', 'Od 850 zł', '2017-03-16 11:55:53', NULL),
	(17, 'Proteza całkowita', '1 000,00 zł', '2017-03-16 11:56:01', NULL),
	(18, 'Proteza szkieletowa', '1 500,00 zł', '2017-03-16 11:56:09', NULL),
	(19, 'Proteza częściowa', 'Od 800 zł', '2017-03-16 11:56:15', NULL),
	(20, 'Proteza elastyczna', '1 500,00 zł', '2017-03-16 11:56:23', NULL),
	(21, 'Aparat ortodontyczny ruchomy', 'Cena dobierana indywidualnie do pacjenta', '2017-03-16 11:56:30', NULL),
	(22, 'Aparat ortodontyczny stały', 'Od 1700 zł', '2017-03-16 11:56:37', NULL),
	(23, 'Wybielanie metodą Beyond Polus', '800,00 zł', '2017-03-16 11:56:45', NULL),
	(24, 'Ekstrakcja zęba stałego', 'Od 150 zł', '2017-03-16 11:56:52', NULL),
	(25, 'Ekstrakcja zęba mlecznego', 'Od 50,00 zł', '2017-03-16 11:56:59', NULL),
	(26, 'Wizyta adaptacyjna dziecka', '50 zł', '2017-03-16 11:57:06', NULL);
/*!40000 ALTER TABLE `price_list` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
