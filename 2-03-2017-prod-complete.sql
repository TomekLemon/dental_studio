-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: lemonade.nazwa.pl:3306
-- Czas generowania: 02 Mar 2017, 09:48
-- Wersja serwera: 5.5.53-MariaDB
-- Wersja PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `lemonade_6`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `appointment` datetime DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `phone`, `appointment`, `content`, `created_at`) VALUES
(1, 'Marcin Wójtowicz', 'marcin@lemonadestudio.pl', '609212060', '2016-05-20 00:00:00', 'WIADOMOŚĆ TESTOWA', '2016-05-19 19:34:16'),
(2, 'Marcin Wójtowicz', 'marcin@lemonadestudio.pl', '609212060', '2016-05-20 00:00:00', 'WIADOMOŚĆ TESTOWA', '2016-05-19 19:37:30'),
(3, 'Marcin Wójtowicz', 'marcin@lemonadestudio.pl', '609212060', '2016-05-20 00:00:00', 'WIADOMOŚĆ TESTOWA', '2016-05-19 19:38:14'),
(4, 'Grzegorz Piaszczyński', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-20 00:00:00', 'Wiadomość testowa', '2016-05-20 12:28:31'),
(5, 'Grzegorz Piaszczyński', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-20 00:00:00', 'Wiadomość testowa', '2016-05-20 12:29:20'),
(6, 'Grzegorz Piaszczyński', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-21 00:00:00', 'Wiadomość testowa', '2016-05-20 12:29:35'),
(7, 'Grzegorz Piaszczyński', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-21 00:00:00', 'Wiadomość testowa', '2016-05-20 12:29:37'),
(8, 'Grzegorz Piaszczyński', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-21 00:00:00', 'Wiadomość testowa', '2016-05-20 12:30:47'),
(9, 'Grzesiek', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-20 00:00:00', 'Testowa', '2016-05-20 12:31:44'),
(10, 'Grzesiek', 'grzesiek@lemonadestudio.pl', '669523895', '2016-05-20 00:00:00', 'Testowa', '2016-05-20 12:32:53'),
(11, 'krzysztof maciag', 'kmac@poczta.onet.eu', '604054433', '2016-05-31 00:00:00', 'proszę o przyjęcie-pilne', '2016-05-31 08:44:22'),
(12, 'Joanna Marczyńska', 'jmarczynska1@wp.pl', '602532621', '2016-06-02 00:00:00', 'strasznie boili prosze mnie szybko przyjęć', '2016-06-01 21:58:44'),
(13, 'Beata Bialas', 'bbialas2@icloud.com', '608419565', '2016-09-07 00:00:00', 'Dzien dobry,\r\n Moje nazwisko jest Beata Bialas. Mam kilka pytan dotyczacych stanu moich zebow , ktore chcialabym jak najdluzej utrzymac, jezeli jest to mozliwe. Chcialabym uzyskac konkretna odpowiedz czy panstwo zgodzilibyscie sie na podjecie leczenia (zaineresowana bym byla na wizyty wrzesien/pazdziernik).  Chcialabym zeby wszystko co jest potrzebne do leczenia moich zebow otrzymac w jednym gabinecie.  Zalezy mi rowniez na czasie (do dwoch miesiecy) czy jest to realne dla panstwa? Prosilabym o przeglad wszystkich zebow. Wiem ze kilka z nich musi byc  powtornie przeleczona kanalowo, niektore maja stare plomby nadajace sie do wymiany, nie wiem czy potrzeba bedzie wstawienia czy wymiany koron? Tak jak wczesniej pisalam wszystko co powinno byc zrobione, czy jest mozliwosc, czy czas pozwoli, ceny (powtorne leczenie kanalu, korona, wymiana plomby)?  Jezeli sie panstwo zgodzicie na leczenie prosze dac mi znac jak sie ustawic z wizytami, z jakim wyprzedzeniem panstwo rejestrujecie, czy mozna by bylo zaczac kilka zebow na raz? Musialabym dojezdzac z Brzozowa dlatego zalezaloby mi rowniez zrobic jak najwiecej przy jednej wizycie. Moge przeslac zdjecia (panoramiczne z 2014 roku oraz zdjecia fmx  zrobione w maju 2016) jezeli panstwo podacie mi  Waszego emaila . Numer telefonu ktory panstwu podalam jest do mojej kolezanki, wolalabym jednak zeby Panstwo kontaktowalo sie ze mna droga emailowa. Jezeli macie panstwo pytania to prosze pisac na email bbialas2@icloud.com, Z gory dziekuje.\r\nz powazaniem\r\nBeata Bialas', '2016-06-08 01:23:15'),
(14, 'Pawel Wardowski', 'pawel.wardowski@gmail.com', '07478421072', '2016-07-12 00:00:00', 'Witam chcialbym umowic sie na konsultacje u panstwa w celu omowienia leczeniaProsze o odpowiedz droga mailową.Dziekuje.Pozdrawiam', '2016-06-09 10:22:22'),
(15, 'Pawel Wardowski', 'pawel.wardowski@gmail.com', '07478421072', '2016-07-12 00:00:00', 'Witam chcialbym umowic sie na konsultacje u panstwa w celu omowienia leczeniaProsze o odpowiedz droga mailową.Dziekuje.Pozdrawiam', '2016-06-09 10:22:29'),
(16, 'Robert Radek', 'robert.radek01@gmail.com', '881703250', '2016-06-25 00:00:00', 'Chciałbym umówić się do dentysty', '2016-06-24 20:52:49'),
(17, 'Gabriela Rydzik', 'gabriela.rydzik@yahoo.com', '0172427215', '2016-06-30 00:00:00', 'Witam,\r\nPisze w zwiazku z praca nad moim zebem, zaplanowana na koniec sierpnia (pierwsza wizyta jest w dniu 24.08 o godzinie 13:00). Podczas rozmowy telefonicznej dr. Marczynska prosila o przeslanie zdjecia zebe, ktore zostalo wykonane w Londynie. Otoz, mam to zdjecie ale nie posiadam adresu emailowego aby je przeslac. Nie sadze aby byla mozliwosc dolaczenia zalacznika w tej wiadomosci..? Prosze o informacje w jaki sposob moge ow zdjecia przeslac.\r\nZ gory dziekuje i pozdrawiam\r\nGabriela\r\ngabriela.rydzik@yahoo.com', '2016-06-30 09:37:53'),
(18, 'Maria Bosak', 'marii@onet.pl', '886541835', '2016-07-04 00:00:00', 'Witam!\r\nMam implant na jedynce, chcę zrobić koronę porcelanową , interesują mnie koszty wykonania. Chcę sie umówic na wizytę , jeśli byłoby to możliwe najlepiej w poniedziałek 04.07.2016r\r\nPozdrawiam i czekam na odpowiedz.', '2016-07-03 19:20:20'),
(19, 'Ewa Sitarz', 'ewka.sitarz@gmail.com', '733333477', '2016-07-18 00:00:00', 'Witam serdecznie, mam pytanie, czy Państwa  gabinet ma podpisaną umowę z NFZ ? Nie mogę znaleźć takiej informacji na stronie. Czy jest też gdzieś cennik na stronie Państwa, też nie mogę zlokalizować?\r\nMoja córka ma 9 miesięcy, ma dwie dolne jedynki i teraz wybiła jej się górna lewa dwójka, wychodzi także górna prawa dwójka, bardzo dziwnie jej się to wybiło, dość że dwójka to w dziwnym miejscu, tak jakby bardziej z przodu, lekarz rodzinny zalecił wizytę u dentysty, czy z takim małym brzdącem też można do Państwa przyjść ? Bardzo proszę o odpowiedź.\r\n\r\nPozdrawiam\r\nEwa', '2016-07-18 13:57:26'),
(20, 'Katarzyna Lenkiewicz', 'katarzyna.lenkiewicz@poczta.fm', '661444229', '2016-08-16 00:00:00', 'Witam!\r\nChciałabym się umówić na wizytę do dr Joanny Marczyńskiej, jestem zainteresowana założeniem licówek kompozytowych na jedynki, aby zamknąć diastemę. Chciałabym zapytać również ile taka usługa kosztuje? Proszę w miarę możliwości o odpowiedź na adres e-mail. Dziękuję i pozdrawiam serdecznie!', '2016-07-28 10:04:58'),
(21, 'Ewelina Kolodziej', 'loczek23@poczta.onet.pl', '+353879375112', '2016-08-19 00:00:00', 'Witam, \r\nchcialabym umowic wizyte u dr Marczynskiej  na 19 sierpnia po godz 10', '2016-08-12 19:42:52'),
(22, 'Ewa Fedko', 'ewa160@wp.pl', '516379464', '2016-08-20 00:00:00', 'Dzień dobry, chciałabym umówić się do Państwa na wizytę w celu konsultacji założenia licówek na zęby.', '2016-08-17 00:43:52'),
(23, 'Ewa Fedko', 'ewa160@wp.pl', '516379464', '2016-08-20 00:00:00', 'Dzień dobry, chciałabym umówić się do Państwa na wizytę w celu konsultacji założenia licówek na zęby.', '2016-08-17 00:44:00'),
(24, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:52'),
(25, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:56'),
(26, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:56'),
(27, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:57'),
(28, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:57'),
(29, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:58'),
(30, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:59'),
(31, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:21:59'),
(32, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:00'),
(33, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:01'),
(34, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:01'),
(35, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:02'),
(36, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:03'),
(37, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:03'),
(38, 'Artur Kiec', 'kawqx1309@gmail.com', '662263405', '2016-09-01 00:00:00', 'Dzien dobry,\r\nNa zębie (czwórka lub piątka) znajduje się lekki biały nalot. Ból odczuwam przy nagryzaniu tego zęba. Nie widzę innych problemów typu próchnica, wszystko wygląda na zdrowe (nie jestem specjalistą, na moje oko :D ) Co to może być? Wiem, że nie da się dokładnie określić choroby zęba, ale mniej więcej na pewno się da i jakim sposobem trzeba go leczyć.\r\n Z góry dziękuję,\r\nPozdrawiam', '2016-08-21 13:22:13'),
(39, 'Sebastian Miłek', 'xlaczek@hotmail.com', '733000037', '2016-09-02 00:00:00', 'Witam ! Chciał bym umówić się na wizytę do Panie Joanna Marczyńska. Zdjęcie lekarstwa i założenie plomby', '2016-08-29 15:50:43'),
(40, 'Anna Kalita', 'anna.kowal@wp.pl', '507597197', '2016-09-10 00:00:00', 'Konsultacja u ortodonty', '2016-08-30 13:23:05'),
(41, 'Justyna Kościółek', 'justyna.guzik@wp.pl', '886778640', '2026-09-24 00:00:00', '6 latek usunięcie lub leczenie mleczakow', '2016-09-08 22:07:52'),
(42, 'Justyna Kościółek', 'justyna.guzik@wp.pl', '886778640', '2026-09-24 00:00:00', '6 latek usunięcie lub leczenie mleczakow', '2016-09-08 22:08:00'),
(43, 'Patryk Kabat', 'patrykabat@gmail.com', '733157485', '2016-10-01 00:00:00', 'Witam. Chcę zapisać się na pierwszą u Państwa wizytę. Planuje założenie aparatu estetycznego. Prosiłbym o wizytę w godzinach 10-11 gdyż muszę dojechać do Rzeszowa.', '2016-09-15 17:16:15'),
(44, 'Patryk Kabat', 'patrykabat@gmail.com', '733157485', '2016-10-01 00:00:00', 'Witam. Chcę zapisać się na pierwszą u Państwa wizytę. Planuje założenie aparatu estetycznego. Prosiłbym o wizytę w godzinach 10-11 gdyż muszę dojechać do Rzeszowa.', '2016-09-15 17:16:18'),
(45, 'Katarzyna Mnich', 'mniszek17@o2.pl', '724944496', '2016-09-22 00:00:00', 'Dzień dobry, chciałabym się zapisać na badanie kontrolne, aby sprawdzić stan moich zębów. Kiedy najwcześniej mogłabym się zgłosić?', '2016-09-21 15:03:49'),
(46, 'Katarzyna Mnich', 'mniszek17@o2.pl', '724944496', '2016-09-22 00:00:00', 'Dzień dobry, chciałabym się zapisać na badanie kontrolne, aby sprawdzić stan moich zębów. Kiedy najwcześniej mogłabym się zgłosić?', '2016-09-21 15:03:54'),
(47, 'anna dudzinska', 'dudzinska.an@gmail.com', '00971507541303', '2016-10-14 00:00:00', 'Interesuje mnie JettPlazma ilifting wampirzy, ktore chcialabym wykonac 14-18 pazdziernika. Jako ze pozniej wyjezdzam z kraju. Bardzo prosze oodpowiedz najlepiej na email. Pozdrawiam!', '2016-10-03 09:22:45'),
(48, 'anna dudzinska', 'dudzinska.an@gmail.com', '00971507541303', '2016-10-14 00:00:00', 'Interesuje mnie JettPlazma ilifting wampirzy, ktore chcialabym wykonac 14-18 pazdziernika. Jako ze pozniej wyjezdzam z kraju. Bardzo prosze oodpowiedz najlepiej na email. Pozdrawiam!', '2016-10-03 09:25:15'),
(49, 'Agata Burchard', 'medeah1801@wp.pl', '535246860', '2016-10-20 00:00:00', 'Witam serdecznie,\r\n\r\n Z tego powodu, że nie mogłam znaleźć adresu e-mail na Państwa stronie pozwoliłam sobie napisać w tym formularzu. Poszukuję pracy na stanowisku recepcjonistki lub pracownika administracji. Gdyby dysponowali Państwo wolnym etatem będę zobowiązana za kontakt i z przyjemnością prześlę Państwu swoje CV.\r\n\r\n\r\nZ pozdrowieniami\r\nAgata Burchard', '2016-10-20 09:57:54'),
(50, 'Oleh Cheberdyn', 'dumkanarodu@i.ua', '734846205', '2016-11-08 00:00:00', 'Plomba zęba', '2016-10-26 21:04:52'),
(51, 'Oleh Cheberdyn', 'dumkanarodu@i.ua', '734846205', '2016-11-08 00:00:00', 'Plomba zęba', '2016-10-26 21:05:02'),
(52, 'dominika kopta', 'dominika.kopta@kidde.com.pl', '605056367', '2016-11-02 00:00:00', 'Dzień dobry,\r\n\r\nchciałbym umówić sie na wizytę z 3letnim dzieckiem, które ma problemy z zabkami. Konieczne jest określenie dobrej metody leczenia. godziny wizyt po 16.30\r\nProszę o pilny kontakt,\r\nDziekuję', '2016-10-29 12:47:37'),
(53, 'Karolina Wilk', 'karolinawilk291096@gmail.com', '07463033332', '2016-12-21 00:00:00', 'Witam! To tak.. Wszystko dzieje sie na górnych zębach. Jedna jedynka jest martwa druga jest leczona kanałowo. Chciałabym w tym miejscu zrobic dwie korony. Mogę wyslav zdjęcia rentgenowskie. Problemem jest to ze przyjeżdżam do Polski 21 grudnia i tylko wtedy mogę zrobic te zeby. Zastanawiam sie tylko czy nadal przyjmujecie pacjentów w tym czasie i czy jest chociaż możliwości zrobienia tych dwóch jedynek. Przepraszam za kłopot i dziękuje za informacje.', '2016-11-29 08:43:06'),
(54, 'Rafał Marciniak', 'rafal@marciniak.rzeszow.pl', '600-094-384', '2017-01-03 00:00:00', 'Bardzo przepraszam, ale muszę odwołać wizytę (konsultacje chirurg, kość szczęki) zaplanowaną na dzień 03-01-2017. Kontaktować się będziemy po dniu 08.01.2017 (last minute wyjazd na święta i Nowy Rok)\r\n\r\nPrzepraszam za kłopot!', '2016-12-17 08:27:12'),
(55, 'Rafał Marciniak', 'rafal@marciniak.rzeszow.pl', '600-094-384', '2017-01-03 00:00:00', 'Bardzo przepraszam, ale muszę odwołać wizytę (konsultacje chirurg, kość szczęki) zaplanowaną na dzień 03-01-2017. Kontaktować się będziemy po dniu 08.01.2017 (last minute wyjazd na święta i Nowy Rok)\r\n\r\nPrzepraszam za kłopot!', '2016-12-17 08:27:16'),
(56, 'Joanna Konkol', 'Joannakonkol1992@gmail.com', '536399565', '2016-12-26 00:00:00', 'Witam serdecznie. Czy jest możliwość zapisania się do Państwa na wizytę w tym tygodniu z bardzo bolacym zębem?  Pozdrawiam', '2016-12-25 17:20:30'),
(57, 'Joanna Konkol', 'Joannakonkol1992@gmail.com', '536399565', '2016-12-26 00:00:00', 'Witam serdecznie. Czy jest możliwość zapisania się do Państwa na wizytę w tym tygodniu z bardzo bolacym zębem?  Pozdrawiam', '2016-12-25 17:20:33'),
(58, 'Magdalena Olejarska', 'lena95@vp.pl', '698328265', '2016-12-30 00:00:00', 'Chciałabym się dowiedzieć czy przyjmują Państwo na leczenie w ramach kontraktu NFZ?', '2016-12-27 22:55:29'),
(59, 'Magdalena Olejarska', 'lena95@vp.pl', '698328265', '2016-12-30 00:00:00', 'Chciałabym się dowiedzieć czy przyjmują Państwo na leczenie w ramach kontraktu NFZ?', '2016-12-27 22:55:34'),
(60, 'Wioleta Kubala', 'kubala.wioleta@gmail.com', '535594449', '2017-01-15 00:00:00', 'Witam,\r\n\r\nJaki jest koszt zbiegu blefaroplastyki górnych powiek?\r\n\r\npozdrawiam,\r\nWioleta Kubala', '2017-01-15 18:15:42'),
(61, 'Damian Warchoł', 'damianwarchol@o2.pl', '536800455', '2017-02-08 00:00:00', 'Założenie plomby', '2017-02-08 02:22:01'),
(62, 'Agnieszka Baran', 'aga670@op.pl', '530374388', '2017-02-09 00:00:00', 'Witam \r\nChciałam zapisać moja córkę 5 letnia na pierwsza wizytę u stomatologa głównie na przegląd jamy ustnej. Najlepiej w godzinach po 16 Dziekuje', '2017-02-09 14:34:27'),
(63, 'Agnieszka Baran', 'aga670@op.pl', '530374388', '2017-02-09 00:00:00', 'Witam \r\nChciałam zapisać moja córkę 5 letnia na pierwsza wizytę u stomatologa głównie na przegląd jamy ustnej. Najlepiej w godzinach po 16 Dziekuje', '2017-02-09 14:34:32'),
(64, 'Piotr Wozniacki', 'p.wozniacki@wp.pl', '+48600917788', '2017-02-18 00:00:00', 'Pani Doktor,\r\nRozsypały mi się te 2 górne jedynki (martwe, rekonstruowane) - tak, że drzazga której nie potrafię wyjąć przeszkadza w jedzeniu. Jeżeli może mnie Pani przyjąć dzisiaj (w sobotę), ale bez dodatkowej opłaty za "ekspres" (mam zator finansowy) - to bardzo proszę o wyznaczenie godziny. Jeżeli nie, to proszę o rezerwację na poniedziałek 20 lutego. Z poważaniem\r\nPiotr Woźniacki', '2017-02-18 09:50:38'),
(65, 'Piotr Wozniacki', 'p.wozniacki@wp.pl', '+48600917788', '2017-02-18 00:00:00', 'Pani Doktor,\r\nRozsypały mi się te 2 górne jedynki (martwe, rekonstruowane) - tak, że drzazga której nie potrafię wyjąć przeszkadza w jedzeniu. Jeżeli może mnie Pani przyjąć dzisiaj (w sobotę), ale bez dodatkowej opłaty za "ekspres" (mam zator finansowy) - to bardzo proszę o wyznaczenie godziny. Jeżeli nie, to proszę o rezerwację na poniedziałek 20 lutego. Z poważaniem\r\nPiotr Woźniacki', '2017-02-18 09:50:57'),
(66, 'Stanislaw Krajewski', 'Stanmet.sk@gmail.com', '889830779', '2017-02-19 00:00:00', 'Mam zaczete leczenie zeba6 a nie skonczone stematolog na zwolnieniu lekarskim stwierdzona przetoka ropna i jest zalozony opatrunek leczenie kanalowe ktorego termin juz uplyol.jaki jest  mozliwy termin zapisania sie na wizyte,jakie by byly koszty leczenia pozdrawiam .', '2017-02-19 18:25:21'),
(67, 'Sabina Rączka', 'geminnia@interia.pl', '661194525', '2017-03-02 00:00:00', 'Jestem pacjentka pani doktor Marczynskiej,w Przeworsku.Pani doktor prosila zebym przyjechała do Niej na krotka wizyte kontrolna do Rzeszowa przed moja nartepna wizyta w Przeworsku ktora odbedzie sie 18.03 2017.Prosilabym o zapisanie mnie na taka wizyte dnia 2.03 gdyz nie dam rady pojawic sie wczesniej,najbardziej odpowiadalo by mi w godzinach połodniowych', '2017-02-22 16:45:11'),
(68, 'Sabina Rączka', 'geminnia@interia.pl', '661194525', '2017-03-02 00:00:00', 'Jestem pacjentka pani doktor Marczynskiej,w Przeworsku.Pani doktor prosila zebym przyjechała do Niej na krotka wizyte kontrolna do Rzeszowa przed moja nartepna wizyta w Przeworsku ktora odbedzie sie 18.03 2017.Prosilabym o zapisanie mnie na taka wizyte dnia 2.03 gdyz nie dam rady pojawic sie wczesniej,najbardziej odpowiadalo by mi w godzinach połodniowych', '2017-02-22 16:45:25');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on_list` tinyint(1) NOT NULL,
  `attachable` tinyint(1) NOT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery`
--

INSERT INTO `gallery` (`id`, `title`, `slug`, `old_slug`, `on_list`, `attachable`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `arrangement`, `created_at`, `updated_at`) VALUES
(1, 'O gabinecie', 'o-gabinecie', 'o-gabinecie', 0, 1, 0, 'O gabinecie', NULL, NULL, NULL, '2016-05-11 10:18:08', '2016-05-11 10:18:08'),
(2, 'Personel przy pracy', 'personel-przy-pracy', 'personel-przy-pracy', 1, 0, 0, 'Pracownicy przy pracy', NULL, NULL, 6, '2016-05-11 10:18:53', '2016-05-20 14:31:48'),
(3, 'Gabinet', 'gabinet', 'gabinet', 1, 1, 0, 'Wnętrze gabinetu', NULL, NULL, 9, '2016-05-11 10:19:20', '2016-05-20 14:31:38'),
(5, 'Stomatologia dziecięca', 'stomatologia-dziecieca', 'stomatologia-dziecieca', 1, 1, 0, 'Stomatologia dziecięca', NULL, NULL, 2, '2016-05-11 10:21:40', '2016-05-20 12:22:50'),
(7, 'Certyfikaty', 'certyfikaty', 'certyfikaty', 1, 0, 0, 'Certyfikaty', NULL, NULL, 7, '2016-05-11 10:38:06', '2016-05-20 14:31:45'),
(8, 'Nasz zespół', 'nasz-zespol', 'nasz-zespol', 1, 1, 0, 'Nasz zespół', NULL, NULL, 8, '2016-05-18 14:17:58', '2016-05-20 14:31:42'),
(9, 'Implantologia', 'implantologia', 'implantologia', 1, 1, 0, 'Implantologia', NULL, NULL, 4, '2016-05-20 00:55:06', '2016-05-20 12:22:33'),
(10, 'Stomatologia  estetyczna', 'stomatologia-estetyczna', 'stomatologia-estetyczna', 1, 1, 0, 'Wybielanie', NULL, NULL, 1, '2016-05-20 03:04:00', '2016-05-20 21:24:34'),
(11, 'Protetyka', 'protetyka', 'protetyka', 0, 1, 0, 'Protetyka', NULL, NULL, NULL, '2016-05-20 03:19:22', '2016-05-20 03:19:22'),
(12, 'Ortodoncja', 'ortodoncja', 'ortodoncja', 1, 1, 0, 'Ortodoncja', NULL, NULL, 3, '2016-05-20 03:29:05', '2016-05-20 12:22:46'),
(13, 'Endodoncja', 'endodoncja', 'endodoncja', 1, 1, 0, 'Endodoncja', NULL, NULL, 5, '2016-05-20 14:28:51', '2016-05-20 14:31:48'),
(14, 'Medycyna estetyczna', 'medycyna-estetyczna', 'medycyna-estetyczna', 1, 1, 0, 'Medycyna estetyczna', NULL, NULL, 10, '2016-05-26 12:30:44', '2016-05-26 12:30:45'),
(15, 'Chirurgia', 'chirurgia', 'chirurgia', 1, 1, 0, 'Chirurgia', NULL, NULL, 11, '2016-08-05 11:48:32', '2016-08-05 11:48:34');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `gallery_photo`
--

CREATE TABLE `gallery_photo` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `gallery_photo`
--

INSERT INTO `gallery_photo` (`id`, `gallery_id`, `photo`, `arrangement`) VALUES
(7, 1, 'gallery-image-573c4263a0c9e.jpeg', 3),
(8, 1, 'gallery-image-573c460402a74.jpeg', 6),
(9, 1, 'gallery-image-573c449931cf1.jpeg', 2),
(10, 1, 'gallery-image-573c44c548c96.jpeg', 1),
(11, 1, 'gallery-image-573c450178a2e.jpeg', 5),
(25, 1, 'gallery-image-573c457224fbd.jpg', 4),
(26, 7, 'gallery-image-573c46d2b0333.jpg', 5),
(27, 7, 'gallery-image-573c46d31eb4f.jpg', 2),
(28, 7, 'gallery-image-573c46d35bbb5.jpg', 3),
(29, 7, 'gallery-image-573c46d3b0681.jpg', 1),
(30, 7, 'gallery-image-573c46d60680e.jpg', 6),
(31, 7, 'gallery-image-573c46d634ddf.jpg', 4),
(32, 7, 'gallery-image-573c46d67aaca.jpg', 7),
(33, 7, 'gallery-image-573c46d6a626a.jpg', 8),
(34, 7, 'gallery-image-573c46d6d30db.jpg', 9),
(35, 7, 'gallery-image-573c46d7254fc.jpg', 10),
(36, 7, 'gallery-image-573c46d75d105.jpg', 11),
(37, 7, 'gallery-image-573c46d782ae6.jpg', 12),
(38, 7, 'gallery-image-573c46d7bb089.jpg', 13),
(39, 7, 'gallery-image-573c46d7f0d88.jpg', 14),
(45, 2, 'gallery-image-573c4de85459f.jpg', 2),
(47, 2, 'gallery-image-573c4de942b41.jpg', 1),
(48, 2, 'gallery-image-573c4de9ae6cf.jpg', 3),
(49, 2, 'gallery-image-573c4dea30045.jpg', 8),
(50, 2, 'gallery-image-573c4dea95b88.jpg', 4),
(51, 2, 'gallery-image-573c4deaf0bef.jpg', 10),
(53, 2, 'gallery-image-573c4debd4d64.jpg', 5),
(54, 5, 'gallery-image-573ee2fc258d4.jpeg', 2),
(55, 5, 'gallery-image-573c4ffd0427f.jpg', 8),
(56, 5, 'gallery-image-573c4ffd592bc.jpg', 9),
(57, 5, 'gallery-image-573c4ffda10df.jpg', 10),
(58, 5, 'gallery-image-573c4ffddc124.jpg', 5),
(61, 5, 'gallery-image-573c50a5286fc.jpg', 6),
(62, 5, 'gallery-image-573c50a586370.jpg', 7),
(63, 3, 'gallery-image-573c516667c1b.jpg', 2),
(64, 3, 'gallery-image-573c5166a50c1.jpg', 1),
(65, 3, 'gallery-image-573c5166dc651.jpg', 5),
(66, 3, 'gallery-image-573c516722003.jpg', 10),
(67, 3, 'gallery-image-573c516767a0c.jpg', 6),
(68, 3, 'gallery-image-573c5167b3b6f.jpg', 7),
(69, 3, 'gallery-image-573c516805b95.jpg', 8),
(70, 3, 'gallery-image-573c516840da0.jpg', 9),
(71, 3, 'gallery-image-573c51688025c.jpg', 3),
(72, 3, 'gallery-image-573c5168c0cbe.jpg', 4),
(74, 8, 'gallery-image-573c5d9f0b27d.jpg', 1),
(75, 8, 'gallery-image-573c5d9f40386.jpg', 3),
(76, 8, 'gallery-image-573c5d9f775f8.jpg', 2),
(77, 8, 'gallery-image-573c5d9fc3966.jpg', 4),
(84, 10, 'gallery-image-573e6295c263e.jpg', 4),
(85, 10, 'gallery-image-573e6295e8e43.jpg', 3),
(86, 10, 'gallery-image-573e629613361.jpg', 2),
(87, 9, 'gallery-image-573e64163893c.jpg', 1),
(88, 9, 'gallery-image-573e64165c97f.jpg', 2),
(89, 9, 'gallery-image-573e641676c1e.jpg', 3),
(91, 11, 'gallery-image-573e663726a34.jpg', 1),
(92, 12, 'gallery-image-573e6871926a0.jpg', 3),
(94, 2, 'gallery-image-573e6a860a77a.jpg', 7),
(95, 2, 'gallery-image-573e6a862c13f.jpg', 6),
(96, 2, 'gallery-image-573e6a864e9ac.jpg', 9),
(97, 2, 'gallery-image-573e6ced893f7.jpg', 12),
(98, 2, 'gallery-image-573e6cedaebc7.jpg', 11),
(99, 2, 'gallery-image-573e6cedc9b4d.jpg', 13),
(100, 5, 'gallery-image-573e6d26e78c8.jpg', 4),
(101, 5, 'gallery-image-573e6d332dd3a.jpg', 3),
(102, 12, 'gallery-image-573edeb3e2a48.jpg', 4),
(103, 12, 'gallery-image-573edeb41b1c2.jpg', 2),
(105, 10, 'gallery-image-573ee197b0b6b.jpg', 1),
(107, 13, 'gallery-image-573f031e556f5.jpg', 4),
(108, 13, 'gallery-image-573f031e945bd.jpg', 2),
(109, 13, 'gallery-image-573f031ec74a0.jpg', 3),
(110, 13, 'gallery-image-573f031f08608.jpg', 1),
(111, 13, 'gallery-image-573f0330180e9.jpg', 5),
(112, 12, 'gallery-image-5742b1fcb2a93.jpg', 5),
(113, 12, 'gallery-image-5742b2303fffd.JPG', 6),
(114, 12, 'gallery-image-5742b23215534.JPG', 7),
(115, 12, 'gallery-image-5742b2648a162.JPG', 8),
(117, 12, 'gallery-image-5742b6cd17d0e.jpg', 9),
(118, 2, 'gallery-image-5742b78507fbe.jpg', 14),
(119, 2, 'gallery-image-5742b796345fb.jpg', 15),
(121, 5, 'gallery-image-5742b7f950e42.jpg', 11),
(122, 2, 'gallery-image-5742b82b7987f.jpg', 16),
(123, 2, 'gallery-image-5742b82c11e15.jpg', 17),
(124, 2, 'gallery-image-5742b82c915e0.jpg', 18),
(126, 12, 'gallery-image-5742b878a6340.jpg', 10),
(127, 12, 'gallery-image-5742c3639c2d0.JPG', 11),
(128, 12, 'gallery-image-5742c36507097.JPG', 12),
(129, 12, 'gallery-image-5742c37f7ac9d.JPG', 13),
(130, 12, 'gallery-image-57457a6e8a585.JPG', 14),
(132, 12, 'gallery-image-57457ab47b942.JPG', 15),
(133, 13, 'gallery-image-57457c48eef94.JPG', 6),
(135, 13, 'gallery-image-57457c65be1d0.JPG', 7),
(136, 9, 'gallery-image-57457d8b7f225.JPG', 4),
(137, 13, 'gallery-image-57457db7d1403.JPG', 8),
(138, 5, 'gallery-image-5745c4128dc13.jpeg', 1),
(139, 14, 'gallery-image-5746d1cb83338.jpg', 1),
(141, 12, 'gallery-image-57485d12b2355.JPG', 16),
(142, 5, 'gallery-image-57485dab1e06c.JPG', 12),
(143, 5, 'gallery-image-57485df6d67a9.JPG', 13),
(144, 5, 'gallery-image-57485df83c9b2.JPG', 14),
(145, 2, 'gallery-image-57485e37507b7.JPG', 19),
(146, 2, 'gallery-image-57485e635969e.JPG', 20),
(147, 12, 'gallery-image-574ff5d4d3850.JPG', 17),
(148, 12, 'gallery-image-574ff5d63e131.JPG', 18),
(149, 12, 'gallery-image-574ff5d7993cf.JPG', 19),
(150, 12, 'gallery-image-574ff716a0f7e.jpg', 20),
(151, 12, 'gallery-image-574ff71748d5d.jpg', 21),
(152, 12, 'gallery-image-574ff717bf72d.jpg', 22),
(153, 12, 'gallery-image-574ff71842768.jpg', 23),
(154, 12, 'gallery-image-574ff767a6b7c.JPG', 24),
(155, 12, 'gallery-image-574ff768efb71.JPG', 25),
(156, 12, 'gallery-image-574ff76a43393.JPG', 26),
(157, 12, 'gallery-image-574ff792d9a46.jpg', 27),
(158, 12, 'gallery-image-574ff793605af.jpg', 28),
(159, 12, 'gallery-image-574ff793d56f3.jpg', 29),
(160, 12, 'gallery-image-574ff79458890.jpg', 30),
(161, 12, 'gallery-image-574ff7bc20339.jpg', 31),
(162, 12, 'gallery-image-574ff7bc98ffe.jpg', 32),
(163, 12, 'gallery-image-574ff7d7adec0.JPG', 33),
(164, 5, 'gallery-image-574ff805c7d53.jpg', 15),
(171, 5, 'gallery-image-574ff8a48743c.JPG', 16),
(172, 5, 'gallery-image-574ff9f6a7fdd.JPG', 17),
(173, 5, 'gallery-image-574ff9f862d72.JPG', 18),
(174, 5, 'gallery-image-574ffa452f909.JPG', 19),
(176, 5, 'gallery-image-574ffa47dc2b5.JPG', 20),
(177, 5, 'gallery-image-574ffabbcdf69.JPG', 21),
(178, 12, 'gallery-image-575596be1d473.JPG', 34),
(179, 12, 'gallery-image-575596bf83a87.JPG', 35),
(180, 12, 'gallery-image-575596eb4ae73.JPG', 36),
(181, 12, 'gallery-image-575596ecaf06e.JPG', 37),
(182, 13, 'gallery-image-575598901dfe4.JPG', 9),
(183, 5, 'gallery-image-5756f0b3bcddf.JPG', 22),
(184, 5, 'gallery-image-5756f0b53d984.JPG', 23),
(185, 5, 'gallery-image-5756f0b67fde1.JPG', 24),
(186, 5, 'gallery-image-5756f0b7bbffe.JPG', 25),
(187, 5, 'gallery-image-5756f0b904fe8.JPG', 26),
(188, 5, 'gallery-image-5756f1048d4b2.JPG', 27),
(189, 5, 'gallery-image-5756f105cebfb.JPG', 28),
(190, 5, 'gallery-image-5756f107209e0.JPG', 29),
(191, 3, 'gallery-image-575a65d5b8027.JPG', 11),
(195, 8, 'gallery-image-575a681d9f944.JPG', 5),
(196, 3, 'gallery-image-575a68576a190.JPG', 12),
(197, 3, 'gallery-image-575a689638d19.JPG', 13),
(198, 5, 'gallery-image-575a6e7a59e2f.JPG', 30),
(199, 5, 'gallery-image-575a6e909f67e.JPG', 31),
(201, 5, 'gallery-image-575a6f7b3acde.JPG', 32),
(202, 5, 'gallery-image-575a6f7c8831e.JPG', 33),
(203, 5, 'gallery-image-5763cc386993b.JPG', 34),
(204, 5, 'gallery-image-5763cc5766504.JPG', 35),
(205, 5, 'gallery-image-5763ccee01aa5.JPG', 36),
(206, 12, 'gallery-image-577a735bb383f.JPG', 38),
(207, 12, 'gallery-image-577a735d4c318.JPG', 39),
(208, 9, 'gallery-image-578ddc64eb732.JPG', 5),
(209, 9, 'gallery-image-578ddc677995c.JPG', 6),
(210, 9, 'gallery-image-578ddc6951e15.JPG', 7),
(211, 9, 'gallery-image-578ddc6b5f9aa.JPG', 8),
(212, 9, 'gallery-image-578ddc6d16705.JPG', 9),
(213, 9, 'gallery-image-578ddc89d82b1.JPG', 10),
(214, 12, 'gallery-image-5796177d52c42.JPG', 40),
(215, 12, 'gallery-image-5796177fbf38a.JPG', 41),
(216, 12, 'gallery-image-579617829dbcd.JPG', 42),
(217, 12, 'gallery-image-579617cddfeae.JPG', 43),
(218, 12, 'gallery-image-579617cf7f059.JPG', 44),
(219, 12, 'gallery-image-57a4609066a08.JPG', 45),
(220, 12, 'gallery-image-57a4609224637.JPG', 46),
(221, 12, 'gallery-image-57a4609399368.JPG', 47),
(222, 12, 'gallery-image-57a46094ee97b.JPG', 48),
(223, 12, 'gallery-image-57a460be0da6f.JPG', 49),
(224, 12, 'gallery-image-57a460bf7ef56.JPG', 50),
(225, 12, 'gallery-image-57bdc978ada20.JPG', 51),
(226, 12, 'gallery-image-57bdc97a538f7.JPG', 52),
(227, 12, 'gallery-image-57bdc97ba9e70.JPG', 53),
(228, 12, 'gallery-image-57bdc9bc1bfa0.JPG', 54),
(229, 12, 'gallery-image-57bdc9bd75783.JPG', 55),
(230, 12, 'gallery-image-57bdc9becd92c.JPG', 56),
(235, 7, 'gallery-image-57c7ec51e8dd4.JPG', 15),
(236, 7, 'gallery-image-57c7ec5342e94.JPG', 16),
(237, 7, 'gallery-image-57c7ec549777d.JPG', 17),
(238, 7, 'gallery-image-57c7ec56461a6.JPG', 18),
(239, 7, 'gallery-image-57c7ec581fcaf.JPG', 19),
(240, 7, 'gallery-image-57c7ec5982852.JPG', 20),
(241, 7, 'gallery-image-57c7ec87b3e18.JPG', 21),
(242, 7, 'gallery-image-57c7ec892084f.JPG', 22),
(248, 5, 'gallery-image-57c7eda973112.JPG', 37),
(249, 5, 'gallery-image-57c7edaae2823.JPG', 38),
(250, 5, 'gallery-image-57c7edac40505.JPG', 39),
(251, 14, 'gallery-image-57cdb658735ee.png', 2),
(252, 14, 'gallery-image-57cdb658d0e8d.png', 3),
(253, 14, 'gallery-image-57cdb6591ce7a.png', 4),
(254, 14, 'gallery-image-57cdb65978805.png', 5),
(255, 14, 'gallery-image-57cdb659b7dbc.png', 6),
(256, 14, 'gallery-image-57cdb659f2755.png', 7),
(257, 14, 'gallery-image-57cdb65a3fbf7.png', 8),
(258, 14, 'gallery-image-57cdb65a81945.png', 9),
(259, 14, 'gallery-image-57cdb65ad1a90.png', 10),
(260, 14, 'gallery-image-57cdb65b38864.png', 11),
(261, 14, 'gallery-image-57cdb65b93ef1.png', 12),
(262, 14, 'gallery-image-57cdb65c28d39.png', 13),
(263, 14, 'gallery-image-57cdb6d3caa76.jpg', 14),
(264, 10, 'gallery-image-57d6977899a28.png', 5),
(265, 5, 'gallery-image-57da53720e2e7.JPG', 40),
(266, 5, 'gallery-image-57da53739bb2c.JPG', 41),
(267, 5, 'gallery-image-57da537507f83.JPG', 42),
(268, 5, 'gallery-image-57da53766544a.JPG', 43),
(269, 5, 'gallery-image-57da5377c4914.JPG', 44),
(270, 5, 'gallery-image-57da53a49de64.JPG', 45),
(271, 5, 'gallery-image-57da53a6106e5.JPG', 46),
(272, 12, 'gallery-image-57da9fa57d902.png', 1),
(273, 12, 'gallery-image-5804cdf93d2c5.JPG', 57),
(274, 12, 'gallery-image-5804cdf9dcad3.JPG', 58),
(275, 12, 'gallery-image-582831e4801ef.jpg', 59),
(276, 12, 'gallery-image-582831e4ea8b4.jpg', 60),
(277, 12, 'gallery-image-582831e549647.jpg', 61),
(278, 12, 'gallery-image-582831e583124.jpg', 62),
(279, 12, 'gallery-image-582831e5ba254.jpg', 63),
(280, 12, 'gallery-image-582832106bb37.jpg', 64),
(281, 12, 'gallery-image-58283210ab42e.jpg', 65),
(282, 12, 'gallery-image-58283210e1bb4.jpg', 66),
(283, 12, 'gallery-image-582832112622b.jpg', 67),
(284, 12, 'gallery-image-582832335b95c.jpg', 68),
(285, 12, 'gallery-image-58283233b25e8.jpg', 69),
(286, 2, 'gallery-image-582832d6108e6.jpg', 21),
(287, 2, 'gallery-image-582832d675173.jpg', 22),
(288, 2, 'gallery-image-582832d6b9444.jpg', 23),
(289, 2, 'gallery-image-582832d6f0596.jpg', 24),
(290, 2, 'gallery-image-582832d731bbf.jpg', 25),
(291, 2, 'gallery-image-582832d76a745.jpg', 26),
(292, 2, 'gallery-image-582832d7a0008.jpg', 27),
(293, 2, 'gallery-image-582832d7d664f.jpg', 28),
(294, 14, 'gallery-image-582c47b1986f8.jpg', 15),
(295, 14, 'gallery-image-582c47b1e8b45.jpg', 16),
(296, 14, 'gallery-image-582c47b20f77e.jpg', 17),
(297, 14, 'gallery-image-582c47b229fe4.jpg', 18),
(298, 14, 'gallery-image-58317f957a700.jpg', 19),
(299, 13, 'gallery-image-584e5f8eadbde.JPG', 10),
(300, 13, 'gallery-image-584e5f8f644a4.JPG', 11),
(301, 13, 'gallery-image-584e5f90225b3.JPG', 12),
(302, 2, 'gallery-image-584e600634297.JPG', 29),
(303, 2, 'gallery-image-584e6006e1853.JPG', 30),
(304, 2, 'gallery-image-584e60078f035.JPG', 31),
(305, 14, 'gallery-image-58aab2f5d6451.jpg', 20),
(306, 14, 'gallery-image-58aab2fe20567.jpg', 21),
(307, 14, 'gallery-image-58aab2fe44848.jpg', 22),
(308, 14, 'gallery-image-58aab3421539a.png', 23),
(309, 14, 'gallery-image-58aab37c62ede.jpg', 24);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu_item`
--

CREATE TABLE `menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route_parameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onclick` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blank` tinyint(1) DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `menu_item`
--

INSERT INTO `menu_item` (`id`, `parent_id`, `location`, `type`, `title`, `route`, `route_parameters`, `url`, `onclick`, `blank`, `arrangement`) VALUES
(1, NULL, 'menu_top', 'route', 'O gabinecie', 'ls_page_show', '{"slug":"o-gabinecie"}', NULL, NULL, 0, 2),
(2, NULL, 'menu_top', 'route', 'Oferta', 'ls_offer', NULL, '#', NULL, 0, 3),
(3, NULL, 'menu_top', 'route', 'Galeria', 'ls_gallery', NULL, NULL, NULL, 0, 5),
(4, NULL, 'menu_top', 'route', 'Nasz zespół', 'ls_person', NULL, '#', NULL, 0, 6),
(5, NULL, 'menu_top', 'route', 'Kontakt', 'ls_contact', NULL, NULL, NULL, 0, 7),
(6, NULL, 'menu_mobile', 'route', 'O gabinecie', 'ls_page_show', '{"slug":"o-gabinecie"}', NULL, NULL, 0, 2),
(7, NULL, 'menu_mobile', 'route', 'Aktualności', 'ls_news', NULL, NULL, NULL, 0, 1),
(8, NULL, 'menu_mobile', 'route', 'Oferta', 'ls_offer', NULL, '#', NULL, 0, 3),
(9, NULL, 'menu_mobile', 'route', 'Galeria', 'ls_gallery', NULL, NULL, NULL, 0, 5),
(10, NULL, 'menu_mobile', 'route', 'Nasz zespół', 'ls_person', NULL, '#', NULL, 0, 6),
(11, NULL, 'menu_mobile', 'route', 'Kontakt', 'ls_contact', NULL, NULL, NULL, 0, 8),
(12, NULL, 'menu_bottom', 'route', 'O gabinecie', 'ls_page_show', '{"slug":"o-gabinecie"}', NULL, NULL, 0, 2),
(13, NULL, 'menu_bottom', 'route', 'Oferta', 'ls_offer', NULL, '#', NULL, 0, 3),
(14, NULL, 'menu_bottom', 'route', 'Galeria', 'ls_gallery', NULL, NULL, NULL, 0, 4),
(15, 14, 'menu_bottom', 'route', 'Certyfikaty', 'ls_gallery_show', '{"slug":"certyfikaty"}', '#', NULL, 0, 3),
(16, NULL, 'menu_bottom', 'route', 'Nasz zespół', 'ls_person', NULL, '#', NULL, 0, 5),
(17, NULL, 'menu_bottom', 'route', 'Kontakt', 'ls_contact', NULL, '#', NULL, 0, 6),
(20, 13, 'menu_bottom', 'route', 'Stomatologia zachowawcza', 'ls_offer_show', '{"slug":"stomatologia-zachowawcza"}', '#', NULL, 0, 1),
(21, 13, 'menu_bottom', 'route', 'Ortodoncja', 'ls_offer_show', '{"slug":"ortodoncja"}', '#', NULL, 0, 4),
(22, 13, 'menu_bottom', 'route', 'Protetyka', 'ls_offer_show', '{"slug":"protetyka"}', '#', NULL, 0, 3),
(23, 13, 'menu_bottom', 'route', 'Stomatologia dziecięca', 'ls_offer_show', '{"slug":"stomatologia-dziecieca"}', '#', NULL, 0, 2),
(24, 13, 'menu_bottom', 'route', 'Implantologia', 'ls_offer_show', '{"slug":"implantologia"}', '#', NULL, 0, 5),
(26, 14, 'menu_bottom', 'route', 'Gabinet', 'ls_gallery_show', '{"slug":"gabinet"}', '#', NULL, 0, 1),
(27, 14, 'menu_bottom', 'route', 'Stomatologia dziecięca', 'ls_gallery_show', '{"slug":"stomatologia-dziecieca"}', '#', NULL, 0, 8),
(29, 14, 'menu_bottom', 'route', 'Personel przy pracy', 'ls_gallery_show', '{"slug":"personel-przy-pracy"}', '#', NULL, 0, 4),
(30, NULL, 'menu_top', 'route', 'Aktualności', 'ls_news', '{"slug":"dla-lekarzy"}', NULL, NULL, 0, 1),
(31, 12, 'menu_bottom', 'route', 'Dla lekarzy', 'ls_page_show', '{"slug":"dla-lekarzy"}', NULL, NULL, 0, 1),
(32, NULL, 'menu_mobile', 'route', 'Dla lekarzy', 'ls_page_show', '{"slug":"dla-lekarzy"}', NULL, NULL, 0, 7),
(33, 13, 'menu_bottom', 'route', 'Endodoncja', 'ls_offer_show', '{"slug":"endodoncja"}', NULL, NULL, 0, 6),
(34, 14, 'menu_bottom', 'route', 'Nasz zespół', 'ls_gallery_show', '{"slug":"nasz-zespol"}', NULL, NULL, 0, 2),
(35, 14, 'menu_bottom', 'route', 'Implantologia', 'ls_gallery_show', '{"slug":"implantologia"}', NULL, NULL, 0, 5),
(36, 14, 'menu_bottom', 'route', 'Ortodoncja', 'ls_gallery_show', '{"slug":"ortodoncja"}', NULL, NULL, 0, 6),
(37, 14, 'menu_bottom', 'route', 'Endodoncja', 'ls_gallery_show', '{"slug":"endodoncja"}', NULL, NULL, 0, 7),
(38, NULL, 'menu_bottom', 'route', 'Aktualności', 'ls_news', NULL, NULL, NULL, 0, 1),
(39, NULL, 'menu_top', 'route', 'Medycyna estetyczna', 'ls_page_show', '{"slug":"medycyna-estetyczna"}', NULL, NULL, 0, 4),
(40, NULL, 'menu_mobile', 'route', 'Medycyna estetyczna', 'ls_page_show', '{"slug":"medycyna-estetyczna"}', NULL, NULL, 0, 4),
(41, 13, 'menu_bottom', 'route', 'Medycyna estetyczna', 'ls_page_show', '{"slug":"medycyna-estetyczna"}', NULL, NULL, 0, 7),
(42, NULL, 'menu_bottom', 'route', 'Medycyna estetyczna', 'ls_gallery_show', '{"slug":"medycyna-estetyczna"}', NULL, NULL, 0, 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20160428081309'),
('20160508134216'),
('20160508134637'),
('20160511102515'),
('20160516101531'),
('20160517100913'),
('20160517171014'),
('20160518060417'),
('20160518063257'),
('20160518074630'),
('20160519123654');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `gallery_id`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `photo`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `published_at`) VALUES
(2, 3, 'Nowa strona internetowa Dental Studio', 'nowa-strona-internetowa-dental-studio', 'nowa-strona-internetowa-dental-studio', 0, 'Serdecznie Państwa zapraszamy na naszą nową witrynę internetową :). Mamy nadzieję, że przypadnie Państwu do gustu i ułatwi kontakt z naszym gabinetem.', '<p>\r\n	<span style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;">Serdecznie Państwa zapraszamy na naszą nową witrynę internetową :). Mamy nadzieję, że przypadnie Państwu do gustu i ułatwi kontakt z naszym gabinetem.</span><br style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;" />\r\n	<br style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;" />\r\n	<span style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;">Serwis, dla Państwa wygody, został r&oacute;wnież <span class="gray">zoptymalizowany do wyświetlania na urządzeniach mobilnych</span> - tabletach i smartfonach.</span><br style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;" />\r\n	<br style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;" />\r\n	<span style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;">Zachęcamy do kontaktu w sprawie wszelkich uwag i sugestii.</span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span style="color: rgb(38, 50, 56); font-family: arial, sans-serif; font-size: 13px; line-height: 16px;">Konsultacje stomatologiczne - Gratis</span></p>', 'news-image-573d9c863f980.jpeg', 0, 'Nowa strona internetowa Dental Studio', 'Nowa, strona, internetowa, Dental, Studio, Serdecznie, Państwa, zapraszamy, naszą, nową', 'Serdecznie Państwa zapraszamy na naszą nową witrynę internetową :). Mamy nadzieję, że przypadnie Państwu do gustu i ułatwi kontakt z naszym gabinetem.', '2016-05-19 12:55:42', '2016-05-22 10:04:41', '2016-05-19 00:00:00'),
(15, 10, 'Nowość', 'nowosc-1', 'nowosc-1', 0, 'Serdecznie zapraszamy na innowacyjne zabiegi w zakresie: zamykania naczynek, usuwania trądziku oraz likwidacji zmarszczek.', '<p>\r\n	Serdecznie zapraszamy na innowacyjne zabiegi w zakresie: zamykania naczynek, usuwania trądziku oraz likwidacji zmarszczek.</p>\r\n<p>\r\n	Zesp&oacute;ł Dental Studio</p>', 'news-image-58086e7435a49.jpeg', 0, 'Nowość', 'Nowość, Serdecznie, zapraszamy, innowacyjne, zabiegi, zakresie:, zamykania, naczynek, usuwania, trądziku', 'Serdecznie zapraszamy na innowacyjne zabiegi w zakresie: zamykania naczynek, usuwania trądziku oraz likwidacji zmarszczek.', '2016-10-20 09:12:51', '2016-10-20 09:12:52', '2016-10-20 00:00:00'),
(16, 14, 'ZLIKWIDUJ SWOJE ZMARSZCZKI !!!', 'zlikwiduj-swoje-zmarszczki', 'zlikwiduj-swoje-zmarszczki', 0, 'Wielkimi krokami zbliża się czas karnawału, zabaw i spotkań z przyjaciółmi. Proponujemy regenerację skóry i podtrzymanie jej pękna Zapraszamy na konsultacje.', '<p>\r\n	Wielkimi krokami zbliża się czas karnawału, zabaw i spotkań z przyjaci&oacute;łmi. Proponujemy regenerację sk&oacute;ry i podtrzymanie jej naturalnego pękna</p>\r\n<p>\r\n	Zapraszamy na konsultacje. :)</p>', 'news-image-582834f42df60.png', 0, 'Zlikwiduj swoje zmarszczki', 'Zlikwiduj, swoje, zmarszczki, Wielkimi, krokami, zbliża, się, czas, karnawału, zabaw', 'Wielkimi krokami zbliża się czas karnawału, zabaw i spotkań z przyjaciółmi. Proponujemy regenerację skóry i podtrzymanie jej pękna Zapraszamy na konsultacje.', '2016-11-13 10:40:01', '2016-11-13 10:41:18', '2016-11-13 00:00:00'),
(17, NULL, 'Happy Hours w Dental Studio!', 'happy-hours-w-dental-studio', 'happy-hours-w-dental-studio', 0, 'Już od 22.11 we wtorki i piątki od 9:30 do 12:00 Happy Hours. Dla stałych pacjentów 10% rabatu na wypełnienie zębów, dodatkowo dla nowych pacjentów - przegląd GRATIS .', '<p>\r\n	Już od 22.11 we wtorki i piątki od 9:30 do 12:00 <strong>Happy Hours</strong>. Dla stałych pacjent&oacute;w 10% rabatu na wypełnienie zęb&oacute;w, dodatkowo dla nowych pacjent&oacute;w - przegląd GRATIS&nbsp;<img alt="" aria-hidden="1" src="https://www.facebook.com/images/emoji.php/v6/f4c/1/16/1f642.png" />.</p>', 'news-image-583419b041786.png', 0, 'Happy Hours w Dental Studio!', 'Happy, Hours, Dental, Studio!, Już, 2211, wtorki, piątki, 9:30, 12:00', 'Już od 22.11 we wtorki i piątki od 9:30 do 12:00 Happy Hours. Dla stałych pacjentów 10% rabatu na wypełnienie zębów, dodatkowo dla nowych pacjentów - przegląd GRATIS .', '2016-11-22 11:10:53', '2016-11-22 11:10:57', '2016-11-22 00:00:00'),
(18, NULL, 'Promocja', 'promocja', 'promocja', 0, 'Serdecznie zapraszamy na bezpłatne kosultacje ortodontyczne. Promocja trwa do końca roku.', '<p>\r\n	Serdecznie zapraszamy na bezpłatne kosultacje ortodontyczne.</p>\r\n<p>\r\n	Promocja trwa do końca roku.</p>\r\n<p>\r\n	Zesp&oacute;ł Dental Studio</p>', 'news-image-583c1b647ee18.jpeg', 0, 'Promocja', 'Promocja, Serdecznie, zapraszamy, bezpłatne, kosultacje, ortodontyczne, trwa, końca, roku', 'Serdecznie zapraszamy na bezpłatne kosultacje ortodontyczne. Promocja trwa do końca roku.', '2016-11-28 12:56:18', '2016-11-28 12:56:22', '2016-11-28 00:00:00'),
(19, NULL, 'Happy Hours', 'happy-hours', 'happy-hours', 0, 'Uprzejmie informujemy, iż promocja Happy Hours dobiega końca.', '<p>\r\n	Uprzejmie informujemy, iż promocja Happy Hours dobiega końca.&nbsp;</p>\r\n<p>\r\n	Już niedługo kolejne promocje :)</p>\r\n<p>\r\n	&nbsp;</p>', 'news-image-5846b7990f55d.jpeg', 0, 'Happy Hours', 'Happy, Hours, Uprzejmie, informujemy, promocja, dobiega, końca', 'Uprzejmie informujemy, iż promocja Happy Hours dobiega końca.', '2016-12-06 14:05:28', '2016-12-06 14:05:29', '2016-12-06 00:00:00'),
(22, NULL, 'Zapraszamy na zabiegi medycyny estetycznej', 'zapraszamy-na-zabiegi-medycyny-estetycznej', 'zapraszamy-na-zabiegi-medycyny-estetycznej', 0, 'W naszej ofercie znajdą państwo między innymi: redukcję cellulitu, usuwanie rozstępów, nawilżenie i ujędrnienie skóry twarzy. Twoja skóra stanie się jędrna, gładka i rozświetlona.', '<p>\r\n	W naszej ofercie znajdą państwo między innymi:</p>\r\n<ul>\r\n	<li>\r\n		redukcję cellulitu,</li>\r\n	<li>\r\n		usuwanie rozstęp&oacute;w,</li>\r\n	<li>\r\n		nawilżenie i ujędrnienie sk&oacute;ry twarzy.</li>\r\n</ul>\r\n<p>\r\n	<em><strong>Twoja sk&oacute;ra stanie się jędrna, gładka i rozświetlona.</strong></em></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Zapraszamy na zabiegi medycyny estetycznej.</p>', 'news-image-588107a156ae4.png', 0, 'Zapraszamy na zabiegi medycyny estetycznej', 'Zapraszamy, zabiegi, medycyny, estetycznej, naszej, ofercie, znajdą, państwo, między, innymi:', 'W naszej ofercie znajdą państwo między innymi: redukcję cellulitu, usuwanie rozstępów, nawilżenie i ujędrnienie skóry twarzy. Twoja skóra stanie się jędrna, gładka i rozświetlona.', '2017-01-19 19:38:24', '2017-01-19 19:38:26', '2017-01-19 00:00:00');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `photo_list` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_detail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_white` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_violet_big` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon_violet_small` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `offer`
--

INSERT INTO `offer` (`id`, `gallery_id`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `photo_list`, `photo_detail`, `icon_white`, `icon_violet_big`, `icon_violet_small`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`, `arrangement`) VALUES
(1, NULL, 'Stomatologia zachowawcza', 'stomatologia-zachowawcza', 'stomatologia-zachowawcza', 0, 'Czym zajmuje się stomatologia zachowawcza? Dział stomatologii zajmująca się profilaktyką i leczeniem zębów. Najczęstszą chorobą jest próchnica, która powstaje poprzez osadzanie płytki bakteryjnej.', '<p class="header">\r\n	Czym zajmuje się stomatologia zachowawcza?</p>\r\n<div>\r\n	Dział stomatologii zajmujący się profilaktyką i leczeniem zęb&oacute;w. Najczęstszą chorobą jest pr&oacute;chnica, kt&oacute;ra powstaje poprzez osadzanie płytki bakteryjnej. Osadza się ona na powierzchni zęba w wyniku niedokładnego czyszczenia. Pr&oacute;chnica może dotyczyć zar&oacute;wno dzieci, jak dorosłych.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Profilaktyka u dzieci</p>\r\n<div>\r\n	Już od wczesnych chwil życia rodzice powinni dbać o stan higieny jamy ustnej swoich dzieci. Nawet w wieku niemowlęcym pokarm nie powinien zalegać długo po jedzeniu gdyż wytwarza środowisko kwaśne, a więc sprzyjające rozwojowi bakterii. U dzieci bardzo popularna jest tzw. <span class="gray">pr&oacute;chnica butelkowa</span>, kt&oacute;ra powstaje na skutek <span class="gray">picia w nocy mleka oraz spożywania dużej ilości sok&oacute;w</span>. Rodzice powinni zgłaszać się jak najwcześniej ze swoimi pociechami w celach kontrolnych oraz po uzyskanie wskaz&oacute;wek od lekarza stomatologa w sprawie profilaktyki pr&oacute;chnicy.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Profilaktyka u os&oacute;b dorosłych i starszych</p>\r\n<div>\r\n	Profilaktyka u os&oacute;b dorosłych jest tak samo ważna, jak w przypadku dzieci. Do zabieg&oacute;w profilaktycznych u starszych pacjent&oacute;w należy <span class="pink">czyszczenie z kamienia i osadu</span>,<span class="pink"> lakierowanie i lakowanie zęb&oacute;w</span>. O zęby należy dbać każdego dnia unikając spożywania cukr&oacute;w, stosować pastę z fluorem, myć zęby po każdym posiłku lub co najmniej dwa razy dziennie.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Leczenie pr&oacute;chnicy</p>\r\n<div>\r\n	W przypadku stwierdzonej pr&oacute;chnicy ząb jest oczyszczony i wypełniony materiałem kompozytowym. Gdy pr&oacute;chnica jest bardziej zaawansowana dochodzi do obnażenia miazgi zębowej i wtedy należy zastosować leczenie kanałowe. Polega ono na usunięciu martwych tkanek z kanał&oacute;w oraz wypełnienie ich materiałem np. gutaperka. <span class="pink">Dobrze wyleczony kanałowo ząb może służyć pacjentowi długie lata odbudowany kompozytem, wł&oacute;knem szklanym lub ceramiką.</span> Nasz gabinet wyposażony jest w <span class="pink">nowoczesny sprzęt niezbędny do prawidłowego leczenia kanałowego zęba</span>. Posiadamy mikroskop, kt&oacute;ry jest używany w przypadku trudnych warunk&oacute;w anatomicznych lub powt&oacute;rnego leczenia.</div>\r\n<div>\r\n	&nbsp;</div>', 'offer-list-image-573de21904dd9.jpeg', 'offer-detail-image-573de23f9176d.jpeg', 'offer-icon-white-5742d12c198d0.png', 'offer-icon-violet-big-5742d0a9264d1.png', 'offer-icon-violet-small-5742d0a927d63.png', 0, 'Stomatologia zachowawcza', 'Stomatologia, zachowawcza, Czym, zajmuje, się, stomatologia, zachowawcza?, Dział, stomatologii, zajmująca', 'Stomatologia zachowawcza Czym zajmuje się stomatologia zachowawcza? Dział stomatologii zajmująca się profilaktyką i leczeniem zębów. Najczęstszą chorobą jest próchnica, która powstaje poprzez osadzanie płytki bakteryjnej.', '2016-05-18 15:53:05', '2016-05-23 11:45:16', 1),
(2, 5, 'Stomatologia dziecięca', 'stomatologia-dziecieca', 'stomatologia-dziecieca', 0, 'Dzieci to bardzo wymagająca grupa pacjentów. Są bardzo wrażliwe, nie lubią igieł oraz długich wizyt. Biorąc to pod uwagę, u naszych małych pacjentów stosujemy tzw. sedację w podtlenku azotu.', '<div>\r\n	Dzieci to bardzo wymagająca grupa pacjent&oacute;w. Są bardzo wrażliwe, nie lubią igieł oraz długich wizyt. Biorąc to pod uwagę, u naszych małych pacjent&oacute;w stosujemy tzw. <span class="pink">sedację w podtlenku azotu. Jest to metoda całkowicie bezpieczna dla zdrowia dziecka</span>, może być r&oacute;wnież stosowana u dorosłych wykazujących duży strach przez wizytą u stomatologa. Dziecko jest uspokojone, odprężone i wizyta jest przyjemna. Większość małych pacjent&oacute;w lubi wizyty w naszym gabinecie gdyż nie odczuwa b&oacute;lu. <span class="pink">Najbardziej odpowiedni wiek do stosowania sedacji to od 4 roku życia wzwyż. Dla mniejszych pacjent&oacute;w mamy inną formę leczenia &ndash; ozon.</span> Jest to bezinwazyjna metoda leczenia pr&oacute;chnicy. Wizyta jest kr&oacute;tka, bezbolesna i co najważniejsze &ndash; bez wiertła. Mali pacjenci oglądają bajkę, czasem nawet nie zauważając leczenia zęb&oacute;w.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Oferujemy</p>\r\n<ul>\r\n	<li>\r\n		Leczenie ozonem</li>\r\n	<li>\r\n		Leczenie w sedacji</li>\r\n	<li>\r\n		Leczenie w znieczuleniu</li>\r\n</ul>', 'offer-list-image-573c7fca098dd.jpeg', 'offer-detail-image-5745c27d0e304.jpeg', 'offer-icon-white-573c7fca62f86.png', 'offer-icon-violet-big-573c7fca647e3.png', 'offer-icon-violet-small-573c7fca661bc.png', 0, 'Stomatologia zachowawcza', 'Stomatologia, zachowawcza, Dzieci, bardzo, wymagająca, grupa, pacjentów, wrażliwe, lubią, igieł', 'Dzieci to bardzo wymagająca grupa pacjentów. Są bardzo wrażliwe, nie lubią igieł oraz długich wizyt. Biorąc to pod uwagę, u naszych małych pacjentów stosujemy tzw. sedację w podtlenku azotu.', '2016-05-18 16:44:25', '2016-05-25 17:19:25', 2),
(3, 11, 'Protetyka', 'protetyka', 'protetyka', 0, 'Protetyka stomatologiczna To dział stomatologii zajmujący się uzupełnianiem braków zębowych lub rekonstrukcji zębów tak zniszczonych, że odbudowa materiałem kompozytowym nie jest wystarczająca.', '<p class="header">\r\n	Protetyka stomatologiczna</p>\r\n<div>\r\n	To dział stomatologii zajmujący się uzupełnianiem brak&oacute;w zębowych lub rekonstrukcji zęb&oacute;w tak zniszczonych, że odbudowa materiałem kompozytowym nie jest wystarczająca. Jeśli mamy do czynienia z brakami zęb&oacute;w należy jak najwcześniej je uzupełnić. <span class="pink">W miarę upływu czasu pozostałe zęby przejmują pracę tych utraconych i ulegają przeciążeniu</span>, tym samym zaburzając prawidłowy zgryz. Dochodzi do patologicznego starcia, chor&oacute;b stawu skroniowo-żuchwowego, b&oacute;l&oacute;w głowy, złego trawienia oraz <span class="pink">wielu dolegliwości początkowo nie kojarzonych ze złym stanem jamy ustnej</span>.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Protetyka w praktyce</p>\r\n<div>\r\n	W zależności od warunk&oacute;w stosujemy uzupełnienia stałe lub ruchome. Przy planowaniu pracy bierze się pod uwagę warunki zgryzowe oraz upodobania pacjenta. <span class="pink">Prace protetyczne możemy wykonywać w oparciu o własne zęby lub implanty.</span> Prace protetyczne wykonywane w naszym gabinecie są estetyczne i służą na wiele lat.</div>\r\n<p class="header">\r\n	&nbsp;</p>\r\n<p class="header">\r\n	Oferujemy</p>\r\n<ul>\r\n	<li>\r\n		Korony, mosty ceramiczne na podbudowie metalowej i bezmetalowej, &nbsp;protezy ruchome</li>\r\n	<li>\r\n		Protezy szkieletowe klasyczne</li>\r\n	<li>\r\n		Protezy szkieletowe bezklamrowe</li>\r\n</ul>', 'offer-list-image-573c808b5e390.jpeg', 'offer-detail-image-573de25379588.jpeg', 'offer-icon-white-573c808bae4f9.png', 'offer-icon-violet-big-573c808bb0adb.png', 'offer-icon-violet-small-573c808bb3bf9.png', 0, 'Protetyka', 'Protetyka, stomatologiczna, dział, stomatologii, zajmujący, się, uzupełnianiem, braków, zębowych, rekonstrukcji', 'Protetyka stomatologiczna To dział stomatologii zajmujący się uzupełnianiem braków zębowych lub rekonstrukcji zębów tak zniszczonych, że odbudowa materiałem kompozytowym nie jest wystarczająca.', '2016-05-18 16:47:38', '2016-05-20 21:45:38', 3),
(4, 12, 'Ortodoncja', 'ortodoncja', 'ortodoncja', 0, 'Czym jest ortodoncja? W naszym gabinecie ortodoncja stosowana jest zarówno u dzieci, jak i u dorosłych. Stosujemy w tym celu szereg aparatów ortopedycznych i ortodontycznych, o najwyższej jakości.', '<p class="header">\r\n	Czym jest ortodoncja?</p>\r\n<div>\r\n	W naszym gabinecie ortodoncja stosowana jest zar&oacute;wno u dzieci, jak i u dorosłych. Stosujemy w tym celu szereg aparat&oacute;w ortopedycznych i ortodontycznych, o najwyższej jakości.</div>\r\n<div>\r\n	W dzisiejszych czasach wady zgryzu są bardzo powszechne. Nieleczone doprowadzają do <span class="pink">patologicznego starcia zęb&oacute;w, stłoczenia, chor&oacute;b stawu skroniowo-żuchwowego oraz licznych dolegliwości ze strony przewodu pokarmowego</span>. Świadomość rodzic&oacute;w o konsekwencji nieleczonego zgryzu jest coraz większa. Dlatego coraz chętniej przyprowadzają swoje dzieci, aby uchronić je przed usuwaniem zęb&oacute;w stałych w wieku nastoletnim i dorosłym.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Aparaty ruchome</p>\r\n<div>\r\n	W pierwszej fazie leczenia stosujemy tzw. aparaty ruchome. Są one kolorowe, estetyczne i dzieci chętnie je noszą. Jeśli wsp&oacute;łpraca z małym pacjentem układa się w spos&oacute;b odpowiedni, leczenie może trwać rok, a potem następuje jego zakończenie lub przejście do następnej fazy &ndash; założenie aparatu stałego.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Aparaty stałe</p>\r\n<div>\r\n	Wśr&oacute;d aparat&oacute;w stałych stosujemy w zależności od wady zgryzu, jak i indywidualnych wymagań pacjent&oacute;w <span class="pink">aparaty klasyczne &ndash; metalowe lub estetyczne</span>. Dla pacjent&oacute;w, kt&oacute;rzy nie mogą korzystać z comiesięcznych wizyt, mamy <span class="pink">aparaty samoligaturujące, przy użyciu kt&oacute;rych wizyty mogą odbywać się raz na kilka miesięcy.</span> Wszystkie możliwości leczenia są przedstawiane pacjentowi na wizycie informacyjnej i decyzję podejmujemy wsp&oacute;lnie mając na celu korzystne leczenie i zadowolenie pacjenta.</div>', 'offer-list-image-573d8488968ea.jpeg', 'offer-detail-image-57486c3572fe7.jpeg', 'offer-icon-white-573d8488eadc5.png', 'offer-icon-violet-big-573d8488ebd13.png', 'offer-icon-violet-small-573d8488f0359.png', 0, 'Ortodoncja', 'Ortodoncja, Czym, jest, ortodoncja?, naszym, gabinecie, ortodoncja, stosowana, zarówno, dzieci', 'Czym jest ortodoncja? W naszym gabinecie ortodoncja stosowana jest zarówno u dzieci, jak i u dorosłych. Stosujemy w tym celu szereg aparatów ortopedycznych i ortodontycznych, o najwyższej jakości.', '2016-05-18 16:48:55', '2016-05-27 17:48:05', 4),
(6, 9, 'Implantologia', 'implantologia', 'implantologia', 0, 'Czym są implanty? Ubytki w uzębieniu są uzupełniane implantami. Jest to śruba, którą wszczepia się w kość i na jej bazie budowana jest widoczna część zęba.', '<p class="header">\r\n	Czym są implanty?</p>\r\n<div>\r\n	Ubytki w uzębieniu są uzupełniane implantami. Jest to śruba, kt&oacute;rą wszczepia się w kość i na jej bazie budowana jest widoczna część zęba. Rozwiązania protetyczne na bazie implant&oacute;w <span class="pink">opracowywane są indywidualnie w zależności od warunk&oacute;w i wymagań pacjenta</span>.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Wprowadzanie implant&oacute;w</p>\r\n<div>\r\n	W zależności od potrzeb oraz warunk&oacute;w zgryzowych oraz kostnych możemy stosować implanty klasyczne oraz jednoczasowe.</div>\r\n<ul>\r\n	<li>\r\n		Klasyczne to takie, kt&oacute;re wszczepia się na jednej wizycie następnie za 3 miesiące przystępuje do budowania na nich części widocznych.</li>\r\n	<li>\r\n		Na implantach jednoczasowych możemy od razu osadzić koronę, zabieg ten nie wymaga dalszych wizyt w gabinecie stomatologicznym. Na implantach możemy planować wiele prac protetycznych jak protezy akrylowe, protezy szkieletowe, mosty i korony. Korony mogą być na podbudowie metalowej lub bezmetalowej.</li>\r\n</ul>', 'offer-list-image-573c82a329cb0.jpeg', 'offer-detail-image-5745c04dada51.jpeg', 'offer-icon-white-573c82a3737e7.png', 'offer-icon-violet-big-573c82a374c1f.png', 'offer-icon-violet-small-573c82a375d3e.png', 0, 'Implantologia', 'Implantologia, Czym, implanty?, Ubytki, uzębieniu, uzupełniane, implantami, Jest, śruba, którą', 'Czym są implanty? Ubytki w uzębieniu są uzupełniane implantami. Jest to śruba, którą wszczepia się w kość i na jej bazie budowana jest widoczna część zęba.', '2016-05-18 16:56:34', '2016-05-25 17:10:05', 5),
(7, 13, 'Endodoncja', 'endodoncja', 'endodoncja', 0, 'Leczenie kanałowe jest wymagane kiedy miazga zęba zostaje zainfekowana lub na skutek urazu .', '<p>\r\n	Leczenie kanałowe jest wymagane kiedy miazga zęba zostaje zainfekowana &nbsp;lub na skutek urazu. Lekarz specjalista za pomocą specjalistycznego sprzętu usuwa chorobowo zmenioną tkankę, wykonuje to pod kontrolą mikroskopu co pozwala mu na dokładny wgląd we wnętrze zęba. Bez tego często nie jest mozliwa dokładna lokalizacja sieci kanał&oacute;w korzeniowych oraz przeszk&oacute;d znajdujacych się wewnątrz zęba.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class="pink"><span class="gray">Dokładnie wyleczony ząb jest najlepszym rozwiązaniem dla pacjenta oraz najtańszym i najwygodniejszym uzupełnieniem protetycznym.</span></span></p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Można na nim zbudować most, &nbsp;zrobić koronę &nbsp;lub po prostu założyć wypełnienie. Leczenie jest bezbolesne, wykonywane w znieczuleniu miejscowym. Nasi lekarze mają do dyspozycji najnowszej generacji sprzęt oraz doświadczenie poparte wieloma szkoleniami.&nbsp;</p>\r\n<p>\r\n	W ramach endodoncji wykonujemy:</p>\r\n<ul>\r\n	<li>\r\n		&nbsp;Leczenie kanałowe pierwotne u dzieci i dorosłych</li>\r\n	<li>\r\n		Powt&oacute;rne leczenie kanałowe ( reendo)</li>\r\n	<li>\r\n		Usuwanie złamanych narzędzi pozostawionych w kanałach korzeniowych</li>\r\n	<li>\r\n		Perforacje korzeni</li>\r\n	<li>\r\n		Leczenie specjalistyczne zęb&oacute;w młodych u dzieci z niedokończonym rozwojem korzeni ( pr&oacute;chnica, uraz)</li>\r\n</ul>', 'offer-list-image-573de2cde103e.jpeg', 'offer-detail-image-573de2ce0bafb.jpeg', 'offer-icon-white-573de2ce1a066.png', 'offer-icon-violet-big-573de2ce1bcb7.png', 'offer-icon-violet-small-573de2ce1cedd.png', 0, 'Endodoncja', 'Endodoncja, Lorem, ipsum, dolor, amet, consectetur, adipiscing, elit, Quisque, elementum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque elementum porta ante, ut lacinia enim interdum quis. Nulla ac blandit metus, vel mattis risus.', '2016-05-19 17:59:09', '2016-05-22 10:40:19', 6),
(8, 10, 'Stomatologia estetyczna', 'stomatologia-estetyczna', 'stomatologia-estetyczna', 0, 'Jest to dział stomatologii zachowawczej i protetyki połączonej ze sobą. Wybielanie zębów jest jak najbardziej częścią tej dziedziny i polega na rozjaśnianiu tkanki zęba. Inne wybielanie wykonujemy dla zębów żywych, inne dla martwych.', '<div>\r\n	Jest to dział stomatologii zachowawczej i protetyki połączonych ze sobą. Wybielanie zęb&oacute;w jest jak najbardziej częścią tej dziedziny i pol<span style="font-size: 15px; line-height: 24px;">ega na rozjaśnianiu tkanki zęba. Inne wybielanie wykonujemy dla zęb&oacute;w żywych, inne dla martwych.&nbsp;</span></div>\r\n<p>\r\n	Są r&oacute;żne metody wybielania zęb&oacute;w, kt&oacute;re omawiane są na wizytach konsultacyjnych gdzie dobieramy spos&oacute;b prowadzenia pacjenta do jego potrzeb. Po wybieleniu zęb&oacute;w pacjent jest gotowy do ewentualnej zmiany niekt&oacute;rych zęb&oacute;w - ich kształtu, uzupełnienia brak&oacute;w. Możemy wtedy prace protetyczne wykonać w nowym kolorze uzyskanym w procesie wybielania. &nbsp;Do zmiany kształtu zęb&oacute;w stosujemy:</p>\r\n<ul>\r\n	<li>\r\n		Korony</li>\r\n	<li>\r\n		Lic&oacute;wki</li>\r\n</ul>\r\n<p>\r\n	Pozwalają one na uzyskanie zadowalającego efektu kosmetycznego. &nbsp;Są one doskonałym rozwiązaniem dla pacjent&oacute;w, kt&oacute;rzy są niezadowoleni z kształtu lub ustawienia swoich zęb&oacute;w, a nie chcącą &nbsp;decydować się na leczenie ortodontyczne. Lic&oacute;wki mogą być:</p>\r\n<ul>\r\n	<li>\r\n		Porcelanowe- wykonywane przez technika</li>\r\n	<li>\r\n		Kompozytowe - zakładane od razu przez lekarza podczas jednej wizyty</li>\r\n</ul>\r\n<div>\r\n	&nbsp;</div>', 'offer-list-image-5742ccc6c9cb0.jpeg', 'offer-detail-image-5745bcffcb418.jpeg', 'offer-icon-white-5742cd100dc30.png', 'offer-icon-violet-big-5742cd100f7b1.png', 'offer-icon-violet-small-5742cd10107ce.png', 0, 'Stomatologia estetyczna', 'Stomatologia, estetyczna, Jest, dział, stomatologii, zachowawczej, protetyki, połączonej, sobą, Wybielanie', 'Jest to dział stomatologii zachowawczej i protetyki połączonej ze sobą. Wybielanie zębów jest jak najbardziej częścią tej dziedziny i polega na rozjaśnianiu tkanki zęba. Inne wybielanie wykonujemy dla zębów żywych, inne dla martwych.', '2016-05-23 11:26:30', '2016-05-26 11:51:11', 7),
(9, NULL, 'Chirurgia stomatologiczna', 'chirurgia-stomatologiczna', 'chirurgia-stomatologiczna', 0, 'Chirurgia stomatologiczna – dziedzina stomatologii zajmująca się drobnymi zabiegami w obrębie wyrostka ekstrakcja ( usunięcie zęba), odsłonięcie zatrzymanych zębów, usunięcie ropni wokół zęba.', '<div>\r\n	<strong>Chirurgia stomatologiczna </strong>&ndash; dziedzina stomatologii zajmująca się drobnymi zabiegami w obrębie wyrostka</div>\r\n<ul>\r\n	<li>\r\n		ekstrakcja ( usunięcie zęba), odsłonięcie zatrzymanych zęb&oacute;w, usunięcie ropni wok&oacute;ł zęba. Jest to rodzaj specjalności, gdzie wykonywane są zabiegi niezbędne przy implantologii, protetyce, czy niekiedy endodoncji. Najczęściej zabiegi wykonywane w gabinecie przez wyspecjalizowany personel pozwalają uniknąć powikłań a tym samym hospitalizacji</li>\r\n	<li>\r\n		&bdquo;operacyjne usuwanie zęb&oacute;w&rdquo; - zabieg, kt&oacute;ry stosowany jest przy niewyrżniętych zębach tzw. zębach zatrzymanych najczęściej tzw. zęby mądrości oraz pozostawione korzenie w zębodole.</li>\r\n</ul>', 'offer-list-image-57a20c433b766.jpeg', 'offer-detail-image-57a20c43eb5b3.jpeg', 'offer-icon-white-57a20c4435667.png', 'offer-icon-violet-big-57a20c4436730.png', 'offer-icon-violet-small-57a20c4437662.png', 0, 'Chirurgia stomatologiczna', 'Chirurgia, stomatologiczna, dziedzina, stomatologii, zajmująca, się, drobnymi, zabiegami, obrębie, wyrostka', 'Chirurgia stomatologiczna – dziedzina stomatologii zajmująca się drobnymi zabiegami w obrębie wyrostka ekstrakcja ( usunięcie zęba), odsłonięcie zatrzymanych zębów, usunięcie ropni wokół zęba.', '2016-08-03 17:22:42', '2016-11-13 21:14:36', 8);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_short_generate` tinyint(1) DEFAULT NULL,
  `content_short` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `seo_generate` tinyint(1) DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `page`
--

INSERT INTO `page` (`id`, `gallery_id`, `title`, `slug`, `old_slug`, `content_short_generate`, `content_short`, `content`, `seo_generate`, `seo_title`, `seo_keywords`, `seo_description`, `created_at`, `updated_at`) VALUES
(1, 1, 'O gabinecie', 'o-gabinecie', 'o-gabinecie', 0, NULL, '<p>\r\n	<span class="gray">Dental Studio to nie tylko gabinet stomatologiczny</span> - to doskonałe połączenie doświadczenia, młodości, stylu i innowacyjności. Jedyną w swoim rodzaju - przyjemną, a zarazem profesjonalną atmosferę wizyt tworzy nasz wysoce wykwalifikowany zesp&oacute;ł, wraz z naszymi najlepszymi przyjaci&oacute;łmi - pacjentami.</p>\r\n<div class="quote" style="undefined">\r\n	<p>\r\n		Doskonałe połączenie doświadczenia, młodości, stylu i innowacyjności</p>\r\n</div>\r\n<p>\r\n	W trzech dostępnych w Dental Studio gabinetach znajdą Państwo pełną i kompleksową opiekę w zakresie ortodoncji, stomatologii zachowawczej, protetyki i endodoncji. Dodatkowo, dysponując najnowocześniejszymi technologiami dostępnymi na rynku, wciąż poszerzamy i ulepszamy wachlarz naszych usług, aby m&oacute;c zapewnić Państwu najwyższy możliwy standard opieki. Podczas wizyty w naszych gabinetach mają Państwo możliwość korzystania z leczenia przy użyciu kamery wewnątrzustnej, mikroskopu, czy też możliwość skorzystania z badań diagnostycznych z użyciem radiowizjografii.</p>\r\n<div class="quote" style="undefined">\r\n	<p>\r\n		Dysponujemy najnowocześniejszymi technologiami dostępnymi na rynku</p>\r\n</div>\r\n<p>\r\n	Dental Studio to r&oacute;wnież miejsce, w kt&oacute;rym nastawieni jesteśmy obok najwyższej jakości usług, r&oacute;wnież na wyeliminowanie stresu często występującego u najmłodszych pacjent&oacute;w. Z uwagi na ich komfort wprowadziliśmy metody leczenia takie jak np. ozon, pozwalający na wyleczenie pr&oacute;chnicy u naszych pociech, bez konieczności borowania. Dodatkowo oferujemy możliwość odbycia wizyty w znieczuleniu podtlenkiem azotu (gazem rozweselającym).</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	<span class="pink">Najmłodsi spędzają czas oczekiwania na wizytę w wypełnionej zabawkami strefie małego pacjenta</span>, po czym przyjmowani są w bajecznie kolorowym gabinecie, zaprojektowanym tak, by mogli się w nim poczuć komfortowo.</p>', 0, 'O gabinecie', 'gabinecie', NULL, '2016-04-28 11:58:14', '2016-05-17 05:26:24'),
(2, NULL, 'Dla lekarzy', 'dla-lekarzy', 'dla-lekarzy', 0, 'Zapraszamy do współpracy lekarzy stomatologów, którzy nie wykonują zabiegów leczenia kanałowego w trudnych przypadkach.', '<div>\r\n	Zapraszamy do wsp&oacute;łpracy lekarzy stomatolog&oacute;w, kt&oacute;rzy nie wykonują zabieg&oacute;w leczenia kanałowego w trudnych przypadkach.&nbsp;Nasza oferta dotyczy:</div>\r\n<ul>\r\n	<li>\r\n		powt&oacute;rnego leczenia kanałowego</li>\r\n	<li>\r\n		usuwania złamanych narzędzi</li>\r\n	<li>\r\n		udrażnianie kanał&oacute;w pod prace protetyczne</li>\r\n	<li>\r\n		wypełniania kanał&oacute;w zakrzywionych</li>\r\n	<li>\r\n		sterylizacja kanał&oacute;w ze zgorzelą laserem.</li>\r\n</ul>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Informujemy, że pacjenci po wykonanym zabiegu są odsyłani do lekarza kierującego.</div>', 0, 'Dla lekarzy', 'lekarzy, Zapraszamy, współpracy, stomatologów, którzy, wykonują, zabiegów, leczenia, kanałowego, trudnych', 'Zapraszamy do współpracy lekarzy stomatologów, którzy nie wykonują zabiegów leczenia kanałowego w trudnych przypadkach.', '2016-05-18 12:07:49', '2016-05-18 12:11:53'),
(3, 14, 'Medycyna estetyczna', 'medycyna-estetyczna', 'medycyna-estetyczna', 0, 'W naszej klinice od niedawna jest możliwość wykonywania niektórych zabiegów z zakresu medycyny estetycznej.', '<p>\r\n	W naszej klinice od niedawna jest możliwość wykonywania niekt&oacute;rych zabieg&oacute;w z zakresu medycyny estetycznej. Są to zabiegi, kt&oacute;re mają na celu:</p>\r\n<ul>\r\n	<li>\r\n		wzmocnienie sk&oacute;ry twarzy, szyji i dekoltu</li>\r\n	<li>\r\n		likwidację zmarszczek trwałych i mimicznych</li>\r\n	<li>\r\n		wzmacnianie włos&oacute;w</li>\r\n	<li>\r\n		konturowanie ust</li>\r\n	<li>\r\n		likwidację niekt&oacute;rych skupisk tłuszczu np. na podbr&oacute;dku.</li>\r\n	<li>\r\n		Blefaroplastyka (plastyka powiek) - NOWOŚĆ</li>\r\n	<li>\r\n		Usuwanie przebarwień</li>\r\n	<li>\r\n		Leczenie trądziku</li>\r\n	<li>\r\n		Lifting sk&oacute;ry</li>\r\n	<li>\r\n		Zamykanie naczynek</li>\r\n	<li>\r\n		Usuwanie blizn</li>\r\n	<li>\r\n		<p>\r\n			Usuwanie rozstęp&oacute;w</p>\r\n	</li>\r\n</ul>\r\n<p>\r\n	Zabiegi przeprowadzane są przez certyfikowaną specjalistkę w tej dziedzinie Joannę Marczyńską. Zapraszamy do bezpłatnych konsultacji!</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Lipoliza</p>\r\n<p>\r\n	To rozpuszczanie tłuszczu , kt&oacute;ry gromadzi się np w okolicy podbr&oacute;dka.Wystarczają 3 zabiegi w odstępach 1- miesięcznych i twarz nabiera subtelnego wyglądu.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Mezoterapia</p>\r\n<p>\r\n	To zabieg polegający na nakłuwaniu twarzy specjalną techniką i wprowadzaniu w sk&oacute;rę koktajli odżywczych lub własnego osocza w celu pobudzenia sk&oacute;ry do produkcji kolagenu. Mezoterapię powtarza sie kilka razy w zależności od potrzeb i daje ona rewelacyjne efekty.&nbsp;<span class="pink">Jest bezpieczna i i już po pierwszym zabiegu sk&oacute;ra zaczyna pracować.</span>&nbsp;Można ją wykonywać pod oczy gdzie sk&oacute;ra jest zawsze bardzo wiotka ,na twarz, i na szyję.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Kwas hialuronowy &ndash; wypełnianie bruzd</p>\r\n<p>\r\n	Z wiekiem zaniedbane zmarszczki utrwalają się i powstają tzw. bruzdy, kt&oacute;re możemy wypełnić kwasem hialuronowym. Służy do wypełniania zmarszczek trwałych i bruzd na twarzy. Istnieją r&oacute;żne stopnie usieciowania kwasu, a zabieg wymaga wcześniejszej konsultacji w celu uzgodnienia przebiegu.&nbsp;</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Likwidacja zmarszczek (kurze łapki, lwia zmarszczka, linie wenus)</p>\r\n<p>\r\n	Likwiduje się je za pomocą toksyny botulinowej. Zabieg wystarcza na 5 do 8 miesięcy i powoduje op&oacute;źnienie starzenia sk&oacute;ry &ndash; jej odpoczynek od pracy w zagrożonym łamaniem miejscu. Najczęściej stosowana w likwidacji tzw. lwiej zmarszczki, kurzych łapek, linii marionetek, pin kod oraz Wenus na szyi.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Bruksizm</p>\r\n<p>\r\n	Jest to stan ciągłej pracy niekt&oacute;rych mięśni twarzy, kt&oacute;ry doprowadza do zniszczenia zęb&oacute;w, wypełnień, a nawet uzupełnień protetycznych. Najczęściej do nadmiernej aktywności tych mięśni dochodzi w nocy kiedy nie mamy nad nimi kontroli. Od niedawna w leczeniu bruksizmu bardzo pomocna jest toksyna botulinowa wstrzykiwana w mięśnie żwacze.&nbsp;<span class="pink">Zabieg jest bezbolesny.</span></p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Wzmacnianie włos&oacute;w</p>\r\n<p>\r\n	Jeśli widzisz , że wypadają Ci włosy to znak, że trzeba coś z tym zrobić.&nbsp;<span class="pink">Mezoterapia głowy jest zabiegiem skutecznym</span>&nbsp;i może być prowadzona w oparciu o użycie własnego osocza lub gotowego koktajlu.&nbsp;<span class="pink">Przynosi efekty po 4 zabiegu.</span>&nbsp;To zabieg polegający na nakłuwaniu sk&oacute;ry głowy specjalną techniką i jednoczesnym wstrzykiwaniu koktajlu lub własnego osocza w celu pobudzenia sk&oacute;ry do produkcji kolagenu. Mezoterapie powtarza się kilka razy w zależności od potrzeby i daje ona rewelacyjne efekty. Jest bezpieczna i i już po pierwszym zabiegu sk&oacute;ra zaczyna pracować.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Nadpotliwość</p>\r\n<p>\r\n	Stres, tempo życia, dużo drobiazg&oacute;w do zrealizowania sprawia że często nadmiernie się pocimy. Jest to niemiłe zjawisko i często utrudnia nam życie, przeszkadza w pracy&nbsp; i powoduje dyskomfort. Jest to bardzo powszechny problem zar&oacute;wno u kobiet, jak i u mężczyzn. W leczeniu zwiększonej potliwości r&oacute;wnież może nam pom&oacute;c toksyna botulinowa, wstrzykiwana w miejsce będące problemem.</p>\r\n<p>\r\n	<span class="pink">Jeśli masz problem z nadmiernym poceniem w r&oacute;żnych okolicach ciała zapraszamy do nas.</span></p>\r\n<p>\r\n	Zastosujemy ostrzykiwanie botoxem i zablokujemy dolegliwość. Ostrzykiwanie można zrobić w każdje okolicy ciała najczęsciej pod pachami, ale r&oacute;wnież na dłoniach, stopach, czole itp.</p>\r\n<div>\r\n	&nbsp;</div>\r\n<p class="header">\r\n	Osocze bogatopłytkowe:</p>\r\n<p>\r\n	To najlepsze co możesz podarować swojej twarzy. Pobrana krew przetworzona jest na koncentrat substancji powodujących tworzenie kolagenu kom&oacute;rek sk&oacute;ry.</p>\r\n<p>\r\n	Mezoterapia szyi, ostrzykiwanie twarzy Twoim osoczem jest bezpiecznym zabiegiem, kt&oacute;ry poprawia kondycję sk&oacute;ry twarzy, zmniejsza zmarszczki, powoduje napięcie i przedłuża młodość. Twarz wygląda młodo i promiennie. Zabieg nie wymaga zwalniania się z pracy.</p>\r\n<p>\r\n	Pobrana krew jest odwirowana w specjalnej wir&oacute;wce. Uzyskujemy w ten spos&oacute;b największy skarb w medycynie estetycznej - żywe płytki krwi, kt&oacute;re mają zdolność do pobudzania ( stymulacji) proces&oacute;w naprawczych sk&oacute;ry.</p>\r\n<p>\r\n	Tak wyprodukowane osocze wstrzykiwane jest w odpowiedni spos&oacute;b w sk&oacute;rę twarzy, szyi, dekoltu itd. Jest to obecnie najskuteczniejsza, bezpieczna i jedna z bardziej popularnych metod odmładzania wszystkich gwiazd, os&oacute;b ze świata show biznesu. Brak przeciwwskazań. Zabieg wykonuje lekarz medycyny estetycznej Joanna Marczyńska.</p>\r\n<div>\r\n	<h3>\r\n		&nbsp;</h3>\r\n	<p class="header">\r\n		Blefaroplastyka powiek, lifting twarzy</p>\r\n	<p>\r\n		Oczy - obszar twarzy, na kt&oacute;rym gł&oacute;wnie skupiamy wzrok i kt&oacute;ry w ogromnym stopniu decyduje o naszej atrakcyjności. Niestety w wyniku proces&oacute;w starzenia zachodzących w sk&oacute;rze pojawiają się zmiany w postaci zmarszczek wok&oacute;ł oczu, nadmiaru sk&oacute;ry na powiekach pozbawionej napięcia. Z czasem opr&oacute;cz dyskomfortu estetycznego (niesymetryczne powieki, znaczny nawis luźnej sk&oacute;ry = smutne spojrzenie ) może pojawić się problem z brakiem pełnego otwarcia oka, ograniczenia pola widzenia itd.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p>\r\n		Medycyna estetyczna oferuje szeroką gamę korekcji opadających powiek. Począwszy od najbardziej inwazyjnych ( chirurgiczna plastyka powiek ) po metody opierające się na zabiegach laserowych, mezoterapii lub aplikacji nici liftingujących. W odpowiedzi na potrzeby wielu pacjent&oacute;w unikających interwencji chirurgicznych, dermatologia &nbsp;zabiegowa znalazła rozwiązanie bez wypełnień, bez igieł, bez skalpela &ndash; czyli nieablacyjną dynamiczną blefaroplastykę sublimacyjną, kt&oacute;ra jest odpowiedzią na metody chirurgiczne plastyki powiek, czyli PLAZMĘ.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<p class="header">\r\n		Plazma</p>\r\n	<p>\r\n		Określana jest jako 4 stan skupienia materii, w kt&oacute;rym elektrony usuwane z atom&oacute;w tworzą zjonizowany gaz. Tworzą się punkty sublimacji w celu uzyskania skurczu sk&oacute;ry i pozbycia się nadmiaru tkanki. Jest to szczeg&oacute;lnie efektywne w zabiegach g&oacute;rnej i dolnej powieki, liftingu twarzy i szyi.</p>\r\n	<p>\r\n		Jako jedyni na podkarpaciu stosujemy to nowatorskie urządzenie, kt&oacute;re wytwarza iskrę zjonizowanego powietrza = plasma. Przepływ iskry następuje na obszarze 1mm2, co jest wielką zaletą ze względu na dokładność w usuwaniu niepożądanych tkanek bez uszkodzenia okolicznych zdrowych. Atutem plazmy jest możliwość zastosowania na powiekę ruchomą i pozwala na obserwację efekt&oacute;w w czasie rzeczywistym, bez przerywania ciągłości sk&oacute;ry właściwej. Możemy uzyskać znaczną poprawę napięcia sk&oacute;ry okolicy oka&nbsp; i w konsekwencji wyeliminowania fałd&oacute;w sk&oacute;rnych, nawis&oacute;w i zmarszczek mimicznych.</p>\r\n	<p>\r\n		&nbsp;</p>\r\n	<div>\r\n		<p>\r\n			<strong>Wskazania</strong></p>\r\n		<ul>\r\n			<li>\r\n				Gł&oacute;wnym wskazaniem generatora plazmy jest nieablacyjna korekta powieki g&oacute;rnej i dolnej.</li>\r\n			<li>\r\n				Lifting twarzy i szyi.</li>\r\n			<li>\r\n				Redukcja płytkich i głębokich zmarszczek na twarzy, szyi i dekolcie.</li>\r\n			<li>\r\n				Redukcja zmarszczek wok&oacute;ł ust.</li>\r\n		</ul>\r\n		<p>\r\n			&nbsp;</p>\r\n	</div>\r\n	<p>\r\n		<strong>Zalety:</strong></p>\r\n	<ul>\r\n		<li>\r\n			Prosta, bezpieczna i kr&oacute;tka procedura około 15 &ndash; 45 min.</li>\r\n		<li>\r\n			Znakomita alternatywa dla metod chirurgicznych plastyki powiek.</li>\r\n		<li>\r\n			Nie powoduje stan&oacute;w zapalnych i uszkodzenia tkanek.</li>\r\n		<li>\r\n			Nie powoduje krwawienia.</li>\r\n		<li>\r\n			Około 70% efekt&oacute;w widoczne jest natychmiast po zabiegu , a końcowy efekt narasta w czasie.</li>\r\n		<li>\r\n			Nie powoduje wyłączenia z codziennej aktywności.</li>\r\n	</ul>\r\n	<p style="margin-bottom: 0cm; line-height: 100%;">\r\n		&nbsp;</p>\r\n	<p>\r\n		<strong>Efekty:</strong></p>\r\n	<ul>\r\n		<li>\r\n			Uniesienie opadających powiek.</li>\r\n		<li>\r\n			Likwidacja zmarszczek wok&oacute;ł oczu.</li>\r\n		<li>\r\n			Poprawa napięcia powieki g&oacute;rnej i dolnej.</li>\r\n		<li>\r\n			Poprawa jędrności sk&oacute;ry twarzy i szyi.</li>\r\n	</ul>\r\n	<p class="header">\r\n		<strong>Wampirzy Lifting</strong></p>\r\n	<p>\r\n		To HIT wśr&oacute;d zabieg&oacute;w odmładzających. Polega na mikronakłuwaniu urządzeniem DERMAPEN i jednoczesnym polewaniu nakłuwanej sk&oacute;ry osoczem uzyskanym z własnej krwi. Jest abiegiem bezbolesnym i trwa około 40 min. Powoduje widoczne napięcie, rozświetlenie sk&oacute;ry., likwidację zmarszczek, zmniejszenie blizn potrądzikowych co w ostatecznym efekcie daje znaczne odmłodzenie sk&oacute;ry. Najczęsciej wkonuje się na twarzy, ale można r&oacute;wnież zastosować też na całym ciele lub tam gdzie jest taka potrzeba.&nbsp; Często pacjentki w ten sposob dbają o szyję ( kt&oacute;ra niestety jest często oznaką upływu czasu), dłonie. Jest zabiegiem bezpiecznym nawet dla alergik&oacute;w gdyż nie mamy tu do czynienia z żadną obcą substancją (wykożystujemy własną krew). Zabieg nie wymaga wyłączenia z codzienności nie ma widocznych siniak&oacute;w, sk&oacute;ra jest lekko zaczerwieniona jak po intensywnym opalaniu.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', 0, 'Medycyna estetyczna', 'Medycyna, estetyczna, naszej, klinice, niedawna, jest, możliwość, wykonywania, niektórych, zabiegów', 'W naszej klinice od niedawna jest możliwość wykonywania niektórych zabiegów z zakresu medycyny estetycznej.', '2016-11-13 21:12:17', '2017-02-20 10:08:39');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `person`
--

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(1022) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrangement` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `person`
--

INSERT INTO `person` (`id`, `title`, `name`, `photo`, `content`, `arrangement`, `created_at`, `updated_at`) VALUES
(1, 'lek. stom.', 'Joanna Marczyńska', 'person-image-573d88748db3f.jpeg', 'Protetyka, ortodoncja.\r\nUkończyła studia na Akademii Medycznej w Krakowie w roku 1985.\r\nWykonuje zabiegi  z medycyny estetycznej.', 1, '2016-05-17 10:46:07', '2016-09-05 21:39:13'),
(3, 'lek. stom.', 'Katarzyna Wędkowska', 'person-image-573d88db78f64.jpeg', 'Endodoncja mikroskopowa.\r\nUkończyła studia na Śląskiej Akademii Medycznej w Katowicach w roku 2006', 2, '2016-05-17 10:57:59', '2016-05-22 13:11:27'),
(4, 'lek. stom.', 'Katarzyna Nowak', 'person-image-573c927152416.jpeg', 'Stomatologia zachowawcza, stomatologia estetyczna.\r\nUkończyła studia na Uniwersytecie Medycznym w Lublinie w roku 2009', 3, '2016-05-17 10:59:33', '2016-05-22 13:09:24'),
(5, 'lek. stom.', 'Maria Kantor', 'person-image-573d894fa2c10.jpeg', 'Stomatologia zachowawcza, stomatologia dziecięca.\r\nUkończyła studia na Uniwersytecie Medycznym w Lublinie w roku 2012', 4, '2016-05-18 17:32:17', '2016-05-22 13:08:23'),
(6, 'Asystentka gabinetu', 'Katarzyna Ziemba', 'person-image-573d89aa22d46.jpeg', 'W 2005 roku uzyskała dyplom Asystentki Stomatologicznej w Medycznej Szkole Zawodowej w Rzeszowie.', 7, '2016-05-18 17:33:56', '2016-05-25 15:11:51'),
(7, 'Asystentka gabinetu', 'Katarzyna Marczyńska', 'person-image-573d89f89beaa.jpeg', 'W 2015 roku uzyskała Dyplom Asystentki Stomatologicznej w Studium Zdrowia i Urody w Rzeszowie.', 8, '2016-05-18 17:38:12', '2016-05-31 17:25:10'),
(8, 'Asystentka gabinetu', 'Beata Leśniak-Styka', 'person-image-573c98852fce7.jpeg', 'W  1983 roku uzyskała dyplom  w Medycznym Studium Zawodowym w Rzeszowie', 9, '2016-05-18 17:38:54', '2016-05-25 15:11:43'),
(9, 'lek. stom.', 'Sylwia Komorowska-Płonka', 'person-image-573c9e6a16abf.jpeg', 'Stomatologia zachowawcza i chirurgia.\r\nUkończyła studia na Akademii Medycznej we Wrocławiu w roku 2009', 5, '2016-05-18 18:55:05', '2016-05-22 13:06:32'),
(10, 'lekarz medycyny i stomatologii', 'Maciej Miczek', 'person-image-5745935a48a86.jpeg', 'Implantologia,\r\nUkończył studia na Akademii Medycznej w Krakowie w roku 1985', 6, '2016-05-25 13:58:17', '2016-05-31 17:23:10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `setting`
--

INSERT INTO `setting` (`id`, `label`, `description`, `value`) VALUES
(1, 'seo_description', 'Domyślna wartość meta tagu "description"', 'Dental Studio / stomatologia Rzeszów - oferujemy leczenie w mikroskopie, stomatologię estetyczną, kompleksową opiekę nad dziećmi. Zapraszamy do naszych specjalistów!'),
(2, 'seo_keywords', 'Domyślna wartość meta tagu "keywords"', 'dentysta, stomatolog, stomatologia, Rzeszów'),
(3, 'seo_title', 'Domyślna wartość meta tagu "title"', 'Dental Studio / stomatologia Rzeszów'),
(4, 'email_to_contact', 'Adres/Adresy e-mail (oddzielone przecinkiem) na które wysyłane są wiadomości z formularza kontaktowego', 'jmarczynska1@wp.pl, rejestracja@dentalstudio.rzeszow.pl,katarzynamarczynska@gmail.com,dentalstudio23@gmail.com'),
(5, 'contact_firm_name_top', 'Kontakt - Nazwa firmy - Top', 'Gabinet Stomatologiczny'),
(6, 'contact_firm_name_bottom', 'Kontakt - Nazwa firmy - Bottom', 'Dental Studio'),
(7, 'contact_phone', 'Kontakt - Numer telefonu', '17 852 11 39'),
(8, 'contact_cellphone', 'Kontakt - Numer telefonu komórkowego', '602 532 621'),
(9, 'contact_address', 'Kontact - Adres', 'Rzeszów, Al. Powstańców Warszawy 18/1'),
(10, 'contact_opening_hours_1', 'Kontakt - Godziny otwarcia - pon-pt', '9:00 - 19:00'),
(11, 'contact_opening_hours_2', 'Kontakt - Godziny otwarcia - sobota', 'do uzgodnienia'),
(12, 'url_youtube', 'Adres kanału na YouTube', '#'),
(13, 'url_facebook', 'Adres strony na Facebook', 'http://www.facebook.com/DentalStudioRzeszow/'),
(14, 'limit_news', 'Ilość aktualności wyświetlanych na jednej stronie listowania', '8'),
(15, 'google_analytics', 'Kod śledzący Google Analytics', '<script>\r\n  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');\r\n\r\n  ga(\'create\', \'UA-31095387-5\', \'auto\');\r\n  ga(\'send\', \'pageview\');\r\n\r\n</script>'),
(16, 'facebook_pixel', 'Kod konwersji Facebook Pixel', '<!-- Facebook Pixel Code -->\r\n<script>\r\n!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?\r\nn.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;\r\nn.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;\r\nt.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,\r\ndocument,\'script\',\'https://connect.facebook.net/en_US/fbevents.js\');\r\n\r\nfbq(\'init\', \'1443681939206618\');\r\nfbq(\'track\', "PageView");</script>\r\n<noscript><img height="1" width="1" style="display:none"\r\nsrc="https://www.facebook.com/tr?id=1443681939206618&ev=PageView&noscript=1"\r\n/></noscript>\r\n<!-- End Facebook Pixel Code -->');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `slider_photo`
--

CREATE TABLE `slider_photo` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arrangement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `slider_photo`
--

INSERT INTO `slider_photo` (`id`, `title`, `filename`, `arrangement`) VALUES
(7, 'Kompleksowa diagnostyka', 'slider-image-573c665d75639.jpeg', 1),
(8, 'Nowoczesne wyposażenie', 'slider-image-573c66c5df1d7.jpeg', 2),
(9, 'Komfortowa poczekalnia', 'slider-image-573c67b885ce4.jpeg', 3),
(10, 'Profesjonalna opieka', 'slider-image-573c67fdcf50c.jpeg', 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `salt`, `password`, `active`, `last_login`, `roles`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'biuro@lemonadestudio.pl', 'e40e0bc6f3ebacbed6e0e19b772845a8', 'Rd/3Txoa82/D9hi5AUbzSaMo0mR7YPs5tVG7TwD12SREfgob9BnzCItP1AhQxf7gi/IHE67DdDYPbMWPZIAROg==', 1, NULL, '["ROLE_ADMIN","ROLE_ALLOWED_TO_SWITCH","ROLE_USER"]', '2015-07-31 13:46:34', NULL),
(2, 'kasia', 'katarzynamarczynska@gmail.com', 'dee55461f47d6703f35f6e044777971f', 'aKyTq13hOUmUhnW3SLt0bjkyj6XfQsigWC2X9m35kUwS2EcqQKFOqMf958rIa3GHXKSIQ7pVSBUAUHmQ+2+XPw==', 1, NULL, '["ROLE_ADMIN"]', '2016-05-20 16:36:52', NULL),
(3, 'pikseo', 'd.rogalska@pikseogroup.pl', 'b72609e6dd3ca0c14db28aeafce0fcf2', 'reCHHsKjnUlWwEpeR8XA5azGbcO413/0ECzhDjamSA74852SdnMmt4EQWpcCFoKxELBkKqKUeoXfSPor+R2A1g==', 1, NULL, '["ROLE_ADMIN"]', '2017-02-15 15:00:09', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_F02A543B4E7AF8F` (`gallery_id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D754D550727ACA70` (`parent_id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_1DD399504E7AF8F` (`gallery_id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_29D6873E4E7AF8F` (`gallery_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_140AB6204E7AF8F` (`gallery_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9F74B898EA750E8` (`label`);

--
-- Indexes for table `slider_photo`
--
ALTER TABLE `slider_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT dla tabeli `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=310;
--
-- AUTO_INCREMENT dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT dla tabeli `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT dla tabeli `slider_photo`
--
ALTER TABLE `slider_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `gallery_photo`
--
ALTER TABLE `gallery_photo`
  ADD CONSTRAINT `FK_F02A543B4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `menu_item`
--
ALTER TABLE `menu_item`
  ADD CONSTRAINT `FK_D754D550727ACA70` FOREIGN KEY (`parent_id`) REFERENCES `menu_item` (`id`);

--
-- Ograniczenia dla tabeli `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD399504E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `FK_29D6873E4E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

--
-- Ograniczenia dla tabeli `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `FK_140AB6204E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
