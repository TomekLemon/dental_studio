<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160518074630 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offer ADD icon_white VARCHAR(255) DEFAULT NULL AFTER photo_detail');
        $this->addSql('ALTER TABLE offer ADD icon_violet_big VARCHAR(255) DEFAULT NULL AFTER icon_white');
        $this->addSql('ALTER TABLE offer ADD icon_violet_small VARCHAR(255) DEFAULT NULL AFTER icon_violet_big');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offer DROP icon_white, DROP icon_violet_big, DROP icon_violet_small');
    }
}
