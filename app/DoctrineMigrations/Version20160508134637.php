<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160508134637 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE slider_photo DROP FOREIGN KEY FK_9203C87C2CCC9638');
        $this->addSql('DROP TABLE slider');
        $this->addSql('DROP INDEX IDX_9203C87C2CCC9638 ON slider_photo');
        $this->addSql('ALTER TABLE slider_photo DROP slider_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE slider (id INT AUTO_INCREMENT NOT NULL, `label` VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, photo_width INT NOT NULL, photo_height INT NOT NULL, UNIQUE INDEX UNIQ_CFC71007EA750E8 (`label`), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE slider_photo ADD slider_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE slider_photo ADD CONSTRAINT FK_9203C87C2CCC9638 FOREIGN KEY (slider_id) REFERENCES slider (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_9203C87C2CCC9638 ON slider_photo (slider_id)');
    }
}
