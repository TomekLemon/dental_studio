<?php

namespace Ls\MenuBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Search {

    private $container;
    private $method;

    public function __construct(ContainerInterface $container, $method = '') {
        $this->container = $container;
        $this->method = $method;
    }

    public function search($search = '') {
        switch ($this->method) {
            case 'aktualnosc' :
                return $this->aktualnosc();

            case 'galeria' :
                return $this->galeria();

            case 'oferta' :
                return $this->oferta();

            case 'podstrona' :
                return $this->podstrona();

            default:
                break;
        }

        return array();
    }

    public function aktualnosc($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsNewsBundle:News', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

    public function galeria($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsGalleryBundle:Gallery', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

    public function oferta($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsOfferBundle:Offer', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

    public function podstrona($search = '') {

        $em = $this->container->get('doctrine')->getManager();

        $search = mb_strtolower($search);

        $items = $em->createQueryBuilder()
                ->select('s')
                ->from('LsPageBundle:Page', 's')
                ->where('s.title LIKE :search')
                ->orderBy('s.title', 'ASC')
                ->setParameter('search', '%' . $search . '%')
                ->getQuery()
                ->getResult();

        $ret_arr = array();
        foreach ($items as $item) {
            $ret_arr[] = array(
                'parameters' => array('slug' => $item->getSlug()),
                'title' => $item->getTitle()
            );
        }

        return $ret_arr;
    }

}
