<?php

namespace Ls\MenuBundle\Menu;

use Knp\Menu\FactoryInterface;
use Ls\MenuBundle\Entity\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FrontMenu implements ContainerAwareInterface {

    use ContainerAwareTrait;

    public function managedMenu(FactoryInterface $factory, array $options) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('id', str_replace('_', '-', $options['location']));

        $em = $this->container->get('doctrine')->getManager();
        $menu_items = $em->createQueryBuilder()
            ->select('m', 'c', 'cc')
            ->from('LsMenuBundle:MenuItem', 'm')
            ->leftJoin('m.children', 'c')
            ->leftJoin('c.children', 'cc')
            ->where('m.location = :location')
            ->andWhere('m.parent is NULL')
            ->orderBy('m.arrangement', 'ASC')
            ->setParameter('location', $options['location'])
            ->getQuery()
            ->getResult();

        foreach ($menu_items as $item) {
            $child = $this->buildMenu($item, $menu);
            $children = $item->getChildren();
            if (count($children) > 0) {
                foreach ($children as $subitem) {
                    $subchild = $this->buildMenu($subitem, $child);
                    $subchildren = $subitem->getChildren();
                    if (count($subchildren) > 0) {
                        foreach ($subchildren as $subsubitem) {
                            $this->buildMenu($subsubitem, $subchild);
                        }
                    }
                }
            }
        }

        return $menu;
    }

    /**
     *
     * @param MenuItem $item
     *
     * @return bool
     */
    public function checkItem($item) {
        $item_arr = $this->getMenuItemArray($item);

        if (!$item_arr) {
            return false;
        }

        return true;
    }

    public function buildMenu($item, $menu) {
        $item_arr = $this->getMenuItemArray($item);

        if (false === $item_arr) {
            return null;
        }

        $option = array();
        if (isset($item_arr['uri'])) {
            $option['uri'] = $item_arr['uri'];
        }
        if (isset($item_arr['route'])) {
            $option['route'] = $item_arr['route'];
        }
        if (isset($item_arr['routeParameters'])) {
            $option['routeParameters'] = $item_arr['routeParameters'];
        }
        $option['linkAttributes'] = array_merge(array('title' => $item_arr['title']), $item_arr['linkAttributes']);
        if (count($item_arr['itemAttributes']) > 0) {
            $option['attributes'] = $item_arr['itemAttributes'];
        }

        return $menu->addChild($item_arr['title'], $option);
    }

    public function getMenuItemArray($item) {
        $router = $this->container->get('router');
        $result = array(
            'title' => $item->getTitle(),
            'linkAttributes' => array(),
            'itemAttributes' => array(),
        );

        if (!$item->getUrl() && !$item->getRoute() && !$item->getOnclick()) {
            return false;
        }

        switch ($item->getType()) {
            case 'url':
                $result['uri'] = $item->getUrl();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $result['linkAttributes']['class'] = 'has-submenu';
                        $result['itemAttributes']['class'] = 'has-submenu';
                    }
                    if ($item->getBlank()) {
                        $result['linkAttributes']['target'] = '_blank';
                    }
                }
                break;

            case 'route':
                if ($item->getRoute()) {
                    $result['route'] = $item->getRoute();

                    $parameters = array();
                    foreach ($item->getRouteParametersArray() as $k => $param) {
                        $parameters[$k] = $param;
                    }
                    if (count($parameters) > 0) {
                        $result['routeParameters'] = $parameters;
                    }
                }

                $class = array();
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $class[] = 'has-submenu';
                    }
                    if ($item->getRoute() == 'ls_front_homepage') {
                        $class[] = 'home';
                    }
                }
                if (count($class) > 0) {
                    $result['itemAttributes']['class'] = implode(' ', $class);
                }
                break;

            case
            'button':
                $result['uri'] = '#';
                if ($item instanceof MenuItem) {
                    if (count($item->getChildren()) > 0) {
                        $result['linkAttributes']['class'] = 'has-submenu';
                        $result['itemAttributes']['class'] = 'has-submenu';
                    }
                }
                $result['linkAttributes']['onclick'] = 'return ' . $item->getOnClick() . ';';
                break;

            default:
                break;
        }

        return $result;
    }

}
