<?php

namespace Ls\MenuBundle\Controller;

use Ls\MenuBundle\Entity\MenuItem;
use Ls\MenuBundle\Form\MenuItemType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $session = $this->container->get('session');
        $locations = $this->container->getParameter('ls_menu.locations');

        if ($session->has('menu_active_tab')) {
            $tab = $session->get('menu_active_tab');
        } else {
            $tab = $locations[0]['label'];
            $session->set('menu_active_tab', $tab);
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Menu', $this->get('router')->generate('ls_admin_menu'));

        $menus = array();
        foreach ($locations as $location) {
            $menus[$location['label']]['name'] = $location['name'];
            $menus[$location['label']]['items'] = $em->createQueryBuilder()
                ->select('m', 'c', 'cc')
                ->from('LsMenuBundle:MenuItem', 'm')
                ->leftJoin('m.children', 'c')
                ->leftJoin('c.children', 'cc')
                ->where('m.location = :location')
                ->andWhere('m.parent is NULL')
                ->orderBy('m.arrangement', 'ASC')
                ->setParameter('location', $location['label'])
                ->getQuery()
                ->getResult();
        }

        return $this->render('LsMenuBundle:Admin:index.html.twig', array(
            'menus' => $menus,
            'tab' => $tab,
        ));
    }

    public function newAction(Request $request) {
        $location = $request->request->get('location');

        $entity = new MenuItem();
        $entity->setLocation($location);

        $form = $this->createForm(MenuItemType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_menu_create'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Dodaj'));

        $response = array(
            'result' => 'OK',
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsMenuBundle:Admin:new.html.twig', array(
                'form' => $form->createView()
            ))->getContent()),
        );

        return new JsonResponse($response);
    }

    private function getMaxKolejnosc($location) {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.location = :location')
            ->andWhere($qb->expr()->isNull('c.parent'))
            ->setParameter('location', $location)
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $entity = new MenuItem();

        $form = $this->createForm(MenuItemType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_menu_create'),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Dodaj'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $entity->setArrangement($this->getMaxKolejnosc($entity->getLocation()));
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            if ($this->isXmlHttpRequest()) {
                return $this->renderJson(array(
                    'result' => 'OK',
                ));
            }

            return $this->redirect($this->generateUrl('ls_admin_menu_edit', array('id' => $entity->getId())));
        }

        return $this->render('LsMenuBundle:Admin:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMenuBundle:MenuItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MenuItem entity.');
        }

        $form = $this->createForm(MenuItemType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_menu_update', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));

        $response = array(
            'result' => 'OK',
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsMenuBundle:Admin:edit.html.twig', array(
                'form' => $form->createView(),
                'entity' => $entity
            ))->getContent()),
        );

        return new JsonResponse($response);
    }

    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LsMenuBundle:MenuItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MenuItem entity.');
        }

        $form = $this->createForm(MenuItemType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_menu_update', array('id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            if ($this->isXmlHttpRequest()) {
                return $this->renderJson(array(
                    'result' => 'OK',
                ));
            }

            return $this->redirect($this->generateUrl('ls_admin_menu_edit', array('id' => $entity->getId())));
        }

        return $this->render('LsMenuBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'entity' => $entity
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();

        if ($this->isXmlHttpRequest()) {
            $entity = $em->getRepository('LsMenuBundle:MenuItem')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MenuItem entity.');
            }

            $em->remove($entity);
            $em->flush();

            $sitemap = $this->get('ls_core.sitemap');
            $sitemap->generate();

            return $this->renderJson(array(
                'result' => 'OK',
            ));
        }
    }

    public function routeParametersAction(Request $request) {
        $route = $request->request->get('route');
        $parameters = $request->request->get('parameters');
        $search = $request->request->get('search');

        $services = $this->getSearchServices();

        $html = '';

        if (array_key_exists($route, $services)) {
            $service = $services[$route];
            if ($service != null) {
                if ($this->container->has($service)) {
                    $elements = $this->container->get($service)->search($search);

                    if (is_array($elements)) {
                        $html = $this->get('templating')->render('LsMenuBundle:Admin:routeparameters_choice.html.twig', array(
                            'parameters' => $parameters,
                            'elements' => $elements
                        ));
                    }
                }
            }
        }

        $response = new Response();
        $response->setContent($html);

        return $response;
    }

    private function updateChildren($elements, $parent_id) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('LsMenuBundle:MenuItem');
        $parent = $repository->find($parent_id);

        $arrangement = 1;
        foreach ($elements as $element) {
            $item = $repository->find($element->id);
            if ($item) {
                $item->setArrangement($arrangement);
                $item->setParent($parent);
                $em->flush();

                if (isset($element->children)) {
                    $this->updateChildren($element->children, $element->id);
                }
                $arrangement++;
            }
        }

        return null;
    }

    public function saveOrderAction(Request $request) {
        $elements = json_decode($request->request->get('elements'));
        $arrangement = 1;

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('LsMenuBundle:MenuItem');

        if (is_array($elements)) {
            foreach ($elements as $element) {
                $item = $repository->find($element->id);
                if ($item) {
                    $item->setArrangement($arrangement);
                    $item->setParent(null);
                    $em->flush();

                    if (isset($element->children)) {
                        $this->updateChildren($element->children, $element->id);
                    }
                    $arrangement++;
                }
            }
        }

        return new Response('OK');
    }

    public function setTabAction(Request $request) {
        $session = $this->container->get('session');

        $tab = $request->request->get('tab');
        $session->set('menu_active_tab', $tab);

        return new Response('OK');
    }

    public function getSearchServices() {
        $config_modules = $this->container->getParameter('ls_menu.modules');

        $services = array();
        foreach ($config_modules as $module) {
            $services[$module['route']] = $module['get_elements_service'];
        }

        return $services;
    }

    protected function renderJson($data, $status = 200, $headers = array()) {
        // fake content-type so browser does not show the download popup when this
        // response is rendered through an iframe (used by the jquery.form.js plugin)
        //  => don't know yet if it is the best solution
        if ($this->get('request')->get('_xml_http_request') && strpos($this->get('request')->headers->get('Content-Type'), 'multipart/form-data') === 0) {
            $headers['Content-Type'] = 'text/plain';
        } else {
            $headers['Content-Type'] = 'application/json';
        }

        return new Response(json_encode($data), $status, $headers);
    }

    protected function isXmlHttpRequest() {
        return $this->get('request')->isXmlHttpRequest() || $this->get('request')->get('_xml_http_request');
    }
}
