<?php

namespace Ls\MenuBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class MenuItemType extends AbstractType {
    private $container;

    public function __construct(ContainerInterface $serviceContainer) {
        $this->container = $serviceContainer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('type', ChoiceType::class, array(
            'choices' => $this->getTypeChoices(),
            'choices_as_values' => true,
            'label' => 'Typ linku'
        ));
        $builder->add('url', null, array(
            'label' => 'URL',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole',
                    'groups' => array(
                        'URL'
                    )
                ))
            )
        ));
        $builder->add('route', ChoiceType::class, array(
            'required' => false,
            'choices' => $this->getModuleChoices(),
            'choices_as_values' => true,
            'placeholder' => '-- Wybierz --',
            'label' => 'Moduł',
            'constraints' => array(
                new NotNull(array(
                    'message' => 'Wybierz opcję',
                    'groups' => array(
                        'Modul'
                    )
                ))
            )
        ));
        $builder->add('route_parameters', HiddenType::class, array(
            'required' => false
        ));
        $builder->add('onclick', ChoiceType::class, array(
            'required' => false,
            'choices' => $this->getOnClickChoices(),
            'choices_as_values' => true,
            'label' => 'Otwierany obiekt',
            'constraints' => array(
                new NotNull(array(
                    'message' => 'Wybierz opcję',
                    'groups' => array(
                        'Przycisk'
                    )
                ))
            )
        ));
        $builder->add('searchServices', HiddenType::class, array(
            'data' => $this->getSearchServicesJson(),
            'mapped' => false
        ), array(
            'style' => ''
        ));
        $builder->add('location', HiddenType::class, array());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\MenuBundle\Entity\MenuItem',
            'validation_groups' => function (FormInterface $form) {
                $type = $form->get('type')->getData();
                $groups = array('Default');
                switch ($type) {
                    case 'url':
                        $groups[] = 'URL';
                        break;

                    case 'route':
                        $groups[] = 'Modul';
                        break;

                    case 'button':
                        $groups[] = 'Przycisk';
                        break;

                    default:
                        break;
                }

                return $groups;
            },
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_menuitem';
    }

    public function getTypeChoices() {
        $types = array(
            'Adres URL' => 'url',
            'Moduł' => 'route',
//            'Przycisk JavaScript' => 'button'
        );

        return $types;
    }

    public function getLocationChoices() {
        $config_locations = $this->container->getParameter('ls_menu.locations');

        $locations = array();
        foreach ($config_locations as $location) {
            $locations[$location['label']] = $location['name'];
        }

        return $locations;
    }

    public function getModuleChoices() {
        $config_modules = $this->container->getParameter('ls_menu.modules');

        $modules = array();
        foreach ($config_modules as $module) {
            $modules[$module['label']] = $module['route'];
        }

        return $modules;
    }

    public function getOnClickChoices() {
        $config_onclick = $this->container->getParameter('ls_menu.onclick');

        $functions = array();
        foreach ($config_onclick as $function) {
            $functions[$function['label']] = $function['name'];
        }

        return $functions;
    }

    public function getSearchServices() {
        $config_modules = $this->container->getParameter('ls_menu.modules');

        $services = array();
        foreach ($config_modules as $module) {
            $services[$module['route']] = $module['get_elements_service'];
        }

        return $services;
    }

    public function getSearchServicesJson() {
        return json_encode($this->getSearchServices());
    }

}
