<?php

namespace Ls\SliderBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class SliderPhotoType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();
            $size = $object->getThumbSize('full');

            if (!$object || null === $object->getId()) {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height'],
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                        ))
                    )
                ));
            } else {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => $size['width'],
                            'minHeight' => $size['height'],
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż ' . $size['width'] . 'px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż ' . $size['height'] . 'px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\SliderBundle\Entity\SliderPhoto',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_slider_photo';
    }
}
