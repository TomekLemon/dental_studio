<?php

namespace Ls\SliderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Ls\CoreBundle\Utils\Tools;

/**
 * SliderPhoto
 * @ORM\Table(name="slider_photo")
 * @ORM\Entity
 */
class SliderPhoto {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $filename;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;

    protected $fullWidth  = 1920;
    protected $fullHeight = 705;
    protected $middleWidth  = 1454;
    protected $middlwHeight = 555;
    protected $smallWidth  = 816;
    protected $smallHeight = 277;

    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return SliderPhoto
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return SliderPhoto
     */
    public function setFilename($filename) {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename() {
        return $this->filename;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return SliderPhoto
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    public function __toString() {
        if (is_null($this->getFilename())) {
            return 'NULL';
        }
        return $this->getFilename();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'full':
                $size['width'] = $this->fullWidth;
                $size['height'] = $this->fullHeight;
                break;
            case 'middle':
                $size['width'] = $this->middleWidth;
                $size['height'] = $this->middlwHeight;
                break;
            case 'small':
                $size['width'] = $this->smallWidth;
                $size['height'] = $this->smallHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->filename)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'full':
                    $sThumbName = Tools::thumbName($this->filename, '_f');
                    break;
                case 'middle':
                    $sThumbName = Tools::thumbName($this->filename, '_m');
                    break;
                case 'small':
                    $sThumbName = Tools::thumbName($this->filename, '_s');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->filename)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'full':
                    $sThumbName = Tools::thumbName($this->filename, '_f');
                    break;
                case 'middle':
                    $sThumbName = Tools::thumbName($this->filename, '_m');
                    break;
                case 'small':
                    $sThumbName = Tools::thumbName($this->filename, '_s');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->filename)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_f = Tools::thumbName($filename, '_f');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_f)) {
                @unlink($filename_f);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->filename) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->filename;
    }

    public function getPhotoWebPath() {
        return empty($this->filename) ? null : '/' . $this->getUploadDir() . '/' . $this->filename;
    }

    public function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    public function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/slider';
    }

    public function upload() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getFilename();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbs();

        unset($this->file);
    }

    public function createThumbs() {
        $sFileName = $this->getFilename();
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameF = Tools::thumbName($sSourceName, '_f');
        $aThumbSizeF = $this->getThumbSize('full');
        $thumb->adaptiveResize($aThumbSizeF['width'] + 3, $aThumbSizeF['height'] + 3);
        $thumb->crop(0, 0, $aThumbSizeF['width'], $aThumbSizeF['height']);
        $thumb->save($sThumbNameF);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameM = Tools::thumbName($sSourceName, '_m');
        $aThumbSizeM = $this->getThumbSize('middle');
        $thumb->adaptiveResize($aThumbSizeM['width'] + 3, $aThumbSizeM['height'] + 3);
        $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
        $thumb->save($sThumbNameM);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameS = Tools::thumbName($sSourceName, '_s');
        $aThumbSizeS = $this->getThumbSize('small');
        $thumb->adaptiveResize($aThumbSizeS['width'] + 3, $aThumbSizeS['height'] + 3);
        $thumb->crop(0, 0, $aThumbSizeS['width'], $aThumbSizeS['height']);
        $thumb->save($sThumbNameS);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getFilename();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 3, $aThumbSize['height'] + 3);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }
}