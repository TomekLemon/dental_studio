<?php

namespace Ls\PersonBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('e')
            ->from('LsPersonBundle:Person', 'e')
            ->orderBy('e.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsPersonBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }
}
