<?php

namespace Ls\PersonBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class PersonType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł'
        ));
        $builder->add('name', null, array(
            'label' => 'Imię i nazwisko',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('content', TextareaType::class, array(
            'label' => 'Opis',
            'attr' => array(
                'rows' => 3
            ),
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();

            if (!$object || null === $object->getId()) {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 280,
                            'minHeight' => 316,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 280px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 316px',
                        ))
                    )
                ));
            } else {
                $form->add('file', FileType::class, array(
                    'label' => 'Nowe zdjęcie',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 280,
                            'minHeight' => 316,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 280px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 316px',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\PersonBundle\Entity\Person',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_person';
    }
}
