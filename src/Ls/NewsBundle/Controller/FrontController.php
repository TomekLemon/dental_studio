<?php

namespace Ls\NewsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * News controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all News entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news')->getValue();
        $allow = 1;

        $today = new \DateTime();
        $today->setTime(23, 59, 59);

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->setParameter('today', $today)
            ->orderBy('a.published_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        return $this->render('LsNewsBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'allow' => $allow
        ));
    }

    public function ajaxMoreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $page = $request->request->get('page');
        $limit = $em->getRepository('LsSettingBundle:Setting')->findOneByLabel('limit_news')->getValue();
        $start = $page * $limit;
        $allow = 1;

        $today = new \DateTime();
        $today->setTime(23, 59, 59);

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->setParameter('today', $today)
            ->orderBy('a.published_at', 'DESC')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (count($entities) < $limit) {
            $allow = 0;
        }

        $response = array(
            'allow' => $allow,
            'html' => iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsNewsBundle:Front:list.html.twig', array(
                'entities' => $entities,
            ))->getContent())
        );

        return new JsonResponse($response);
    }

    /**
     * Finds and displays a News entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsNewsBundle:News', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find News entity.');
        }

        $today = new \DateTime();
        $today->setTime(23, 59, 59);

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->andWhere('a.id != :id')
            ->setParameter('today', $today)
            ->setParameter('id', $entity->getId())
            ->orderBy('a.published_at', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();

        return $this->render('LsNewsBundle:Front:show.html.twig', array(
            'entities' => $entities,
            'entity' => $entity,
        ));
    }
}
