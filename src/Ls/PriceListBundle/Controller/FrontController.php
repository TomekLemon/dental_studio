<?php

namespace Ls\PriceListBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ls\OfferBundle\Entity\Offer;

/**
 * PriceList controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all PriceList entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        
        $offers = $em->createQueryBuilder()
            ->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->orderBy('a.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        $entities = $em->createQueryBuilder()
            ->select('a')
            ->from('LsPriceListBundle:PriceList', 'a')
            ->getQuery()
            ->getResult();

        return $this->render('LsPriceListBundle:Front:index.html.twig', array(
            'entities' => $entities,
            'offers' => $offers
        ));
    }

    /**
     * Finds and displays a PriceList entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsPriceListBundle:PriceList', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find PriceList entity.');
        }

        $entities = $em->createQueryBuilder()
            ->select('a')
            ->from('LsPriceListBundle:PriceList', 'a')
            ->orderBy('a.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsPriceListBundle:Front:show.html.twig', array(
            'entities' => $entities,
            'entity' => $entity,
        ));
    }
}
