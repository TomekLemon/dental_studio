<?php

namespace Ls\PriceListBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CoreBundle\Utils\Tools;
use Ls\PriceListBundle\Entity\PriceList;

class PriceListUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PriceList) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PriceList) {
            $em->persist($entity);
            $em->flush();
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PriceList) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PriceList) {
            $em->persist($entity);
            $em->flush();
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof PriceList) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsPriceListBundle:PriceList', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->setParameter('arrangement', $arrangement)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsPriceListBundle:PriceList';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhotoList();
            $entity->deletePhotoDetail();
            $entity->deleteIconWhite();
            $entity->deleteIconVioletBig();
            $entity->deleteIconVioletSmall();
        }
    }

}