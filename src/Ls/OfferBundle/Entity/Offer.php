<?php

namespace Ls\OfferBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ls\CoreBundle\Utils\Tools;
use Ls\GalleryBundle\Entity\Gallery;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Offer
 * @ORM\Table(name="offer")
 * @ORM\Entity
 */
class Offer {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo_list;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo_detail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $icon_white;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $icon_violet_big;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $icon_violet_small;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Ls\GalleryBundle\Entity\Gallery",
     *     inversedBy="offers"
     * )
     * @ORM\JoinColumn(
     *     name="gallery_id",
     *     referencedColumnName="id",
     *     onDelete="SET NULL"
     * )
     * @var \Ls\GalleryBundle\Entity\Gallery
     *
     */
    private $gallery;

    protected $file_list;
    protected $file_detail;
    protected $file_icon_white;
    protected $file_icon_violet_big;
    protected $file_icon_violet_small;

    protected $listWideWidth    = 600;
    protected $listWideHeight   = 196;
    protected $listNarrowWidth  = 280;
    protected $listNarrowHeight = 196;
    protected $detailWidth      = 280;
    protected $detailHeight     = 316;

    /**
     * Constructor
     */
    public function __construct() {
        $this->content_short_generate = true;
        $this->seo_generate = true;
        $this->created_at = new \DateTime();
        $this->published_at = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Offer
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Offer
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return Offer
     */
    public function setOldSlug($old_slug) {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug() {
        return $this->old_slug;
    }

    /**
     * Set content_short_generate
     *
     * @param boolean $content_short_generate
     * @return Offer
     */
    public function setContentShortGenerate($content_short_generate) {
        $this->content_short_generate = $content_short_generate;

        return $this;
    }

    /**
     * Get content_short_generate
     *
     * @return boolean
     */
    public function getContentShortGenerate() {
        return $this->content_short_generate;
    }

    /**
     * Set content_short
     *
     * @param string $content_short
     * @return Offer
     */
    public function setContentShort($content_short) {
        $this->content_short = $content_short;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Offer
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set photo_list
     *
     * @param string $photo_list
     * @return Offer
     */
    public function setPhotoList($photo_list) {
        $this->photo_list = $photo_list;

        return $this;
    }

    /**
     * Get photo_list
     *
     * @return string
     */
    public function getPhotoList() {
        return $this->photo_list;
    }

    /**
     * Set photo_detail
     *
     * @param string $photo_detail
     * @return Offer
     */
    public function setPhotoDetail($photo_detail) {
        $this->photo_detail = $photo_detail;

        return $this;
    }

    /**
     * Get photo_detail
     *
     * @return string
     */
    public function getPhotoDetail() {
        return $this->photo_detail;
    }

    /**
     * Set icon_white
     *
     * @param string $icon_white
     * @return Offer
     */
    public function setIconWhite($icon_white) {
        $this->icon_white = $icon_white;

        return $this;
    }

    /**
     * Get icon_white
     *
     * @return string
     */
    public function getIconWhite() {
        return $this->icon_white;
    }

    /**
     * Set icon_violet_big
     *
     * @param string $icon_violet_big
     * @return Offer
     */
    public function setIconVioletBig($icon_violet_big) {
        $this->icon_violet_big = $icon_violet_big;

        return $this;
    }

    /**
     * Get icon_violet_big
     *
     * @return string
     */
    public function getIconVioletBig() {
        return $this->icon_violet_big;
    }

    /**
     * Set icon_violet_small
     *
     * @param string $icon_violet_small
     * @return Offer
     */
    public function setIconVioletSmall($icon_violet_small) {
        $this->icon_violet_small = $icon_violet_small;

        return $this;
    }

    /**
     * Get icon_violet_small
     *
     * @return string
     */
    public function getIconVioletSmall() {
        return $this->icon_violet_small;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seo_generate
     * @return Offer
     */
    public function setSeoGenerate($seo_generate) {
        $this->seo_generate = $seo_generate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seo_title
     * @return Offer
     */
    public function setSeoTitle($seo_title) {
        $this->seo_title = $seo_title;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seo_keywords
     * @return Offer
     */
    public function setSeoKeywords($seo_keywords) {
        $this->seo_keywords = $seo_keywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seo_description
     * @return Offer
     */
    public function setSeoDescription($seo_description) {
        $this->seo_description = $seo_description;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Offer
     */
    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Offer
     */
    public function setUpdatedAt($updated_at) {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Offer
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set gallery
     *
     * @param \Ls\GalleryBundle\Entity\Gallery $gallery
     * @return Offer
     */
    public function setGallery(Gallery $gallery = null) {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \Ls\GalleryBundle\Entity\Gallery
     */
    public function getGallery() {
        return $this->gallery;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/offer';
    }

    /* Zdjęcie na listę */

    public function setFileList(UploadedFile $file = null) {
        $this->file_list = $file;
    }

    public function getFileList() {
        return $this->file_list;
    }

    public function getPhotoListAbsolutePath() {
        return empty($this->photo_list) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo_list;
    }

    public function getPhotoListWebPath() {
        return empty($this->photo_list) ? null : '/' . $this->getUploadDir() . '/' . $this->photo_list;
    }

    public function getPhotoListSize() {
        $temp = getimagesize($this->getPhotoListAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function getThumbListSize($type) {
        $size = array();
        switch ($type) {
            case 'list_wide':
                $size['width'] = $this->listWideWidth;
                $size['height'] = $this->listWideHeight;
                break;
            case 'list_narrow':
                $size['width'] = $this->listNarrowWidth;
                $size['height'] = $this->listNarrowHeight;
                break;
        }
        return $size;
    }

    public function getThumbListWebPath($type) {
        if (empty($this->photo_list)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list_wide':
                    $sThumbName = Tools::thumbName($this->photo_list, '_lw');
                    break;
                case 'list_narrow':
                    $sThumbName = Tools::thumbName($this->photo_list, '_ln');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbListAbsolutePath($type) {
        if (empty($this->photo_list)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'list_wide':
                    $sThumbName = Tools::thumbName($this->photo_list, '_lw');
                    break;
                case 'list_narrow':
                    $sThumbName = Tools::thumbName($this->photo_list, '_ln');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function deletePhotoList() {
        if (!empty($this->photo_list)) {
            $filename = $this->getPhotoListAbsolutePath();
            $filename_lw = Tools::thumbName($filename, '_lw');
            $filename_ln = Tools::thumbName($filename, '_ln');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_lw)) {
                @unlink($filename_lw);
            }
            if (file_exists($filename_ln)) {
                @unlink($filename_ln);
            }
        }
    }

    public function uploadPhotoList() {
        if (null === $this->file_list) {
            return;
        }

        $sFileName = $this->getPhotoList();

        $this->file_list->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbsList();

        unset($this->file_list);
    }

    public function createThumbsList() {
        $sFileName = $this->getPhotoList();
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameLW = Tools::thumbName($sSourceName, '_lw');
        $aThumbSizeLW = $this->getThumbListSize('list_wide');
        $thumb->adaptiveResize($aThumbSizeLW['width'] + 2, $aThumbSizeLW['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeLW['width'], $aThumbSizeLW['height']);
        $thumb->save($sThumbNameLW);

        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameLN = Tools::thumbName($sSourceName, '_ln');
        $aThumbSizeLN = $this->getThumbListSize('list_narrow');
        $thumb->adaptiveResize($aThumbSizeLN['width'] + 2, $aThumbSizeLN['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeLN['width'], $aThumbSizeLN['height']);
        $thumb->save($sThumbNameLN);
    }

    public function ThumbList($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhotoList();
        $sThumbName = $this->getThumbListAbsolutePath($type);
        $aThumbSize = $this->getThumbListSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

    /* Zdjęcie do szczegółów */

    public function setFileDetail(UploadedFile $file = null) {
        $this->file_detail = $file;
    }

    public function getFileDetail() {
        return $this->file_detail;
    }

    public function getPhotoDetailAbsolutePath() {
        return empty($this->photo_detail) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo_detail;
    }

    public function getPhotoDetailWebPath() {
        return empty($this->photo_detail) ? null : '/' . $this->getUploadDir() . '/' . $this->photo_detail;
    }

    public function getPhotoDetailSize() {
        $temp = getimagesize($this->getPhotoDetailAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function getThumbDetailSize($type) {
        $size = array();
        switch ($type) {
            case 'detail':
                $size['width'] = $this->detailWidth;
                $size['height'] = $this->detailHeight;
                break;
        }
        return $size;
    }

    public function getThumbDetailWebPath($type) {
        if (empty($this->photo_detail)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo_detail, '_d');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbDetailAbsolutePath($type) {
        if (empty($this->photo_detail)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'detail':
                    $sThumbName = Tools::thumbName($this->photo_detail, '_d');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function deletePhotoDetail() {
        if (!empty($this->photo_detail)) {
            $filename = $this->getPhotoDetailAbsolutePath();
            $filename_d = Tools::thumbName($filename, '_d');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_d)) {
                @unlink($filename_d);
            }
        }
    }

    public function uploadPhotoDetail() {
        if (null === $this->file_detail) {
            return;
        }

        $sFileName = $this->getPhotoDetail();

        $this->file_detail->move($this->getUploadRootDir(), $sFileName);

        $this->createThumbsDetail();

        unset($this->file_detail);
    }

    public function createThumbsDetail() {
        $sFileName = $this->getPhotoDetail();
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = \PhpThumbFactory::create($sSourceName);
        $sThumbNameD = Tools::thumbName($sSourceName, '_d');
        $aThumbSizeD = $this->getThumbDetailSize('detail');
        $thumb->adaptiveResize($aThumbSizeD['width'] + 2, $aThumbSizeD['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeD['width'], $aThumbSizeD['height']);
        $thumb->save($sThumbNameD);
    }

    public function ThumbDetail($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhotoDetail();
        $sThumbName = $this->getThumbDetailAbsolutePath($type);
        $aThumbSize = $this->getThumbDetailSize($type);
        $thumb = \PhpThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

    /* Ikona biała */

    public function setFileIconWhite(UploadedFile $file = null) {
        $this->file_icon_white = $file;
    }

    public function getFileIconWhite() {
        return $this->file_icon_white;
    }

    public function getIconWhiteAbsolutePath() {
        return empty($this->icon_white) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->icon_white;
    }

    public function getIconWhiteWebPath() {
        return empty($this->icon_white) ? null : '/' . $this->getUploadDir() . '/' . $this->icon_white;
    }

    public function deleteIconWhite() {
        if (!empty($this->icon_white)) {
            $filename = $this->getIconWhiteAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function uploadIconWhite() {
        if (null === $this->file_icon_white) {
            return;
        }

        $sFileName = $this->getIconWhite();

        $this->file_icon_white->move($this->getUploadRootDir(), $sFileName);

        unset($this->file_icon_white);
    }

    /* Ikona fioletowa duża */

    public function setFileIconVioletBig(UploadedFile $file = null) {
        $this->file_icon_violet_big = $file;
    }

    public function getFileIconVioletBig() {
        return $this->file_icon_violet_big;
    }

    public function getIconVioletBigAbsolutePath() {
        return empty($this->icon_violet_big) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->icon_violet_big;
    }

    public function getIconVioletBigWebPath() {
        return empty($this->icon_violet_big) ? null : '/' . $this->getUploadDir() . '/' . $this->icon_violet_big;
    }

    public function deleteIconVioletBig() {
        if (!empty($this->icon_violet_big)) {
            $filename = $this->getIconVioletBigAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function uploadIconVioletBig() {
        if (null === $this->file_icon_violet_big) {
            return;
        }

        $sFileName = $this->getIconVioletBig();

        $this->file_icon_violet_big->move($this->getUploadRootDir(), $sFileName);

        unset($this->file_icon_violet_big);
    }

    /* Ikona fioletowa mała */

    public function setFileIconVioletSmall(UploadedFile $file = null) {
        $this->file_icon_violet_small = $file;
    }

    public function getFileIconVioletSmall() {
        return $this->file_icon_violet_small;
    }

    public function getIconVioletSmallAbsolutePath() {
        return empty($this->icon_violet_small) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->icon_violet_small;
    }

    public function getIconVioletSmallWebPath() {
        return empty($this->icon_violet_small) ? null : '/' . $this->getUploadDir() . '/' . $this->icon_violet_small;
    }

    public function deleteIconVioletSmall() {
        if (!empty($this->icon_violet_small)) {
            $filename = $this->getIconVioletSmallAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function uploadIconVioletSmall() {
        if (null === $this->file_icon_violet_small) {
            return;
        }

        $sFileName = $this->getIconVioletSmall();

        $this->file_icon_violet_small->move($this->getUploadRootDir(), $sFileName);

        unset($this->file_icon_violet_small);
    }
}