<?php

namespace Ls\OfferBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

class OfferType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('title', null, array(
            'label' => 'Tytuł',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'Wypełnij pole'
                ))
            )
        ));
        $builder->add('slug', null, array(
            'label' => 'Końcówka adresu URL'
        ));
        $builder->add('gallery', EntityType::class, array(
            'label' => 'Galeria',
            'class' => 'LsGalleryBundle:Gallery',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('g')
                    ->where('g.attachable = 1');
            }
        ));
        $builder->add('content_short_generate', null, array(
            'label' => 'Automatycznie generuj krótką treść'
        ));
        $builder->add('content_short', TextareaType::class, array(
            'label' => 'Krótka treść',
            'attr' => array(
                'rows' => 5
            )
        ));
        $builder->add('content', null, array(
            'label' => 'Treść'
        ));
        $builder->add('seo_generate', null, array(
            'label' => 'Generuj automatycznie'
        ));
        $builder->add('seo_title', null, array(
            'label' => 'Tag "title"'
        ));
        $builder->add('seo_keywords', TextareaType::class, array(
            'label' => 'Meta "keywords"'
        ));
        $builder->add('seo_description', TextareaType::class, array(
            'label' => 'Meta "description"',
            'attr' => array(
                'rows' => 3
            )
        ));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $object = $event->getData();
            $form = $event->getForm();

            if (!$object || null === $object->getId()) {
                $form->add('file_list', FileType::class, array(
                    'label' => 'Nowe zdjęcie - lista',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 600,
                            'minHeight' => 196,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 600px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 196px',
                        ))
                    )
                ));
                $form->add('file_detail', FileType::class, array(
                    'label' => 'Nowe zdjęcie - szczegóły',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 280,
                            'minHeight' => 316,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 280px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 316px',
                        ))
                    )
                ));
                $form->add('file_icon_white', FileType::class, array(
                    'label' => 'Nowe zdjęcie - ikona biała',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 60,
                            'minHeight' => 60,
                            'maxWidth' => 60,
                            'maxHeight' => 60,
                            'mimeTypes' => 'image/png',
                            'minWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'minHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'mimeTypesMessage' => 'Zdjęcie musi być plikiem PNG',
                        ))
                    )
                ));
                $form->add('file_icon_violet_big', FileType::class, array(
                    'label' => 'Nowe zdjęcie - ikona fioletowa duża',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 60,
                            'minHeight' => 60,
                            'maxWidth' => 60,
                            'maxHeight' => 60,
                            'mimeTypes' => 'image/png',
                            'minWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'minHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'mimeTypesMessage' => 'Zdjęcie musi być plikiem PNG',
                        ))
                    )
                ));
                $form->add('file_icon_violet_small', FileType::class, array(
                    'label' => 'Nowe zdjęcie - ikona fioletowa mała',
                    'constraints' => array(
                        new NotBlank(array(
                            'message' => 'Wybierz zdjęcie'
                        )),
                        new Image(array(
                            'minWidth' => 46,
                            'minHeight' => 46,
                            'maxWidth' => 46,
                            'maxHeight' => 46,
                            'mimeTypes' => 'image/png',
                            'minWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'minHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'maxWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'maxHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'mimeTypesMessage' => 'Zdjęcie musi być plikiem PNG',
                        ))
                    )
                ));
            } else {
                $form->add('file_list', FileType::class, array(
                    'label' => 'Nowe zdjęcie - lista',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 600,
                            'minHeight' => 196,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 600px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 196px',
                        ))
                    )
                ));
                $form->add('file_detail', FileType::class, array(
                    'label' => 'Nowe zdjęcie - szczegóły',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 280,
                            'minHeight' => 316,
                            'minWidthMessage' => 'Szerokość zdjęcie musi być większa niż 280px',
                            'minHeightMessage' => 'Wysokość zdjęcie musi być większa niż 316px',
                        ))
                    )
                ));
                $form->add('file_icon_white', FileType::class, array(
                    'label' => 'Nowe zdjęcie - ikona biała',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 60,
                            'minHeight' => 60,
                            'maxWidth' => 60,
                            'maxHeight' => 60,
                            'mimeTypes' => 'image/png',
                            'minWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'minHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'mimeTypesMessage' => 'Zdjęcie musi być plikiem PNG',
                        ))
                    )
                ));
                $form->add('file_icon_violet_big', FileType::class, array(
                    'label' => 'Nowe zdjęcie - ikona fioletowa duża',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 60,
                            'minHeight' => 60,
                            'maxWidth' => 60,
                            'maxHeight' => 60,
                            'mimeTypes' => 'image/png',
                            'minWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'minHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'maxHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 60x60px',
                            'mimeTypesMessage' => 'Zdjęcie musi być plikiem PNG',
                        ))
                    )
                ));
                $form->add('file_icon_violet_small', FileType::class, array(
                    'label' => 'Nowe zdjęcie - ikona fioletowa mała',
                    'constraints' => array(
                        new Image(array(
                            'minWidth' => 46,
                            'minHeight' => 46,
                            'maxWidth' => 46,
                            'maxHeight' => 46,
                            'mimeTypes' => 'image/png',
                            'minWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'minHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'maxWidthMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'maxHeightMessage' => 'Zdjęcie musi mieć rozdzielczość 46x46px',
                            'mimeTypesMessage' => 'Zdjęcie musi być plikiem PNG',
                        ))
                    )
                ));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ls\OfferBundle\Entity\Offer',
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix() {
        return 'form_admin_offer';
    }
}
