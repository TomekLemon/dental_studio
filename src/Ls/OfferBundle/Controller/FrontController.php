<?php

namespace Ls\OfferBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Offer controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Offer entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->createQueryBuilder()
            ->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->orderBy('a.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsOfferBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Offer entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('LsOfferBundle:Offer', 'p')
            ->where('p.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Offer entity.');
        }

        $entities = $em->createQueryBuilder()
            ->select('a')
            ->from('LsOfferBundle:Offer', 'a')
            ->orderBy('a.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        return $this->render('LsOfferBundle:Front:show.html.twig', array(
            'entities' => $entities,
            'entity' => $entity,
        ));
    }
}
