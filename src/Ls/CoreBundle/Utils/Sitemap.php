<?php

namespace Ls\CoreBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Sitemap {
    private $container;
    private $router;
    private $xml;
    private $em;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->em = $this->container->get('doctrine')->getManager();
        $this->router = $this->container->get('router');
        $this->xml = null;
    }

    public function generate() {
        $this->xml = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' standalone='yes'?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"></urlset>");

        $url = $this->container->get('router')->generate('ls_core_homepage', array(), true);
        $child = $this->xml->addChild('url');
        $child->addChild('loc', $url);

        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_contact')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $url = $this->container->get('router')->generate('ls_contact', array(), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);
        }

        $this->podstrony();
        $this->aktualnosci();

        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_person')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $url = $this->container->get('router')->generate('ls_person', array(), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);
        }

        $this->offer();
        $this->galerie();

        $filename = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'sitemap.xml';

        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($this->xml->asXML());
        $dom->save($filename);
    }

    public function podstrony() {
        $items = $this->em->createQueryBuilder()
            ->select('e')
            ->from('LsPageBundle:Page', 'e')
            ->getQuery()
            ->getResult();

        foreach ($items as $item) {
            $count = $this->em->createQueryBuilder()
                ->select('COUNT(c.id)')
                ->from('LsMenuBundle:MenuItem', 'c')
                ->where('c.route LIKE :route')
                ->andWhere('c.route_parameters LIKE :slug')
                ->setParameter('route', 'ls_page_show')
                ->setParameter('slug', '%' . $item->getSlug() . '%')
                ->getQuery()
                ->getSingleScalarResult();

            if ($count > 0) {
                $url = $this->container->get('router')->generate('ls_page_show', array('slug' => $item->getSlug()), true);
                $child = $this->xml->addChild('url');
                $child->addChild('loc', $url);
                if ($item->getUpdatedAt() != null) {
                    $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
                }
            }
        }
    }

    public function aktualnosci() {
        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_news')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $items = $this->em->createQueryBuilder()
                ->select('e')
                ->from('LsNewsBundle:News', 'e')
                ->orderBy('e.published_at', 'ASC')
                ->getQuery()
                ->getResult();

            $url = $this->container->get('router')->generate('ls_news', array(), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);

            foreach ($items as $item) {
                $url = $this->container->get('router')->generate('ls_news_show', array('slug' => $item->getSlug()), true);
                $child = $this->xml->addChild('url');
                $child->addChild('loc', $url);
                if ($item->getUpdatedAt() != null) {
                    $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
                }
            }
        }
    }

    public function offer() {
        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_offer')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $items = $this->em->createQueryBuilder()
                ->select('e')
                ->from('LsOfferBundle:Offer', 'e')
                ->orderBy('e.arrangement', 'ASC')
                ->getQuery()
                ->getResult();

            $url = $this->container->get('router')->generate('ls_offer', array(), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);

            foreach ($items as $item) {
                $url = $this->container->get('router')->generate('ls_offer_show', array('slug' => $item->getSlug()), true);
                $child = $this->xml->addChild('url');
                $child->addChild('loc', $url);
                if ($item->getUpdatedAt() != null) {
                    $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
                }
            }
        }
    }

    public function galerie() {
        $count = $this->em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsMenuBundle:MenuItem', 'c')
            ->where('c.route = :route')
            ->setParameter('route', 'ls_gallery')
            ->getQuery()
            ->getSingleScalarResult();

        if ($count > 0) {
            $items = $this->em->createQueryBuilder()
                ->select('e')
                ->from('LsGalleryBundle:Gallery', 'e')
                ->where('e.on_list = 1')
                ->getQuery()
                ->getResult();

            $url = $this->container->get('router')->generate('ls_gallery', array(), true);
            $child = $this->xml->addChild('url');
            $child->addChild('loc', $url);

            foreach ($items as $item) {
                $url = $this->container->get('router')->generate('ls_gallery_show', array('slug' => $item->getSlug()), true);
                $child = $this->xml->addChild('url');
                $child->addChild('loc', $url);
                if ($item->getUpdatedAt() != null) {
                    $child->addChild('lastmod', $item->getUpdatedAt()->format(\DateTime::ATOM));
                }
            }
        }
    }
}
