<?php

namespace Ls\CoreBundle\Controller;

use Ls\CoreBundle\Helper\AdminBlock;
use Ls\CoreBundle\Helper\AdminDashboard;
use Ls\CoreBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {
    protected $containerBuilder;

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $slider = $em->createQueryBuilder()
            ->select('e')
            ->from('LsSliderBundle:SliderPhoto', 'e')
            ->orderBy('e.arrangement', 'asc')
            ->getQuery()
            ->getResult();

        $today = new \DateTime();
        $today->setTime(23, 59, 59);

        $qb = $em->createQueryBuilder();
        $news = $qb->select('a')
            ->from('LsNewsBundle:News', 'a')
            ->where($qb->expr()->isNotNull('a.published_at'))
            ->andWhere('a.published_at <= :today')
            ->setParameter('today', $today)
            ->orderBy('a.published_at', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();

        foreach ($news as $entity) {
            $entity->setContentShort(Tools::truncateWord($entity->getContentShort(), 115, '...'));
        }

        $offers = $em->createQueryBuilder()
            ->select('e')
            ->from('LsOfferBundle:Offer', 'e')
            ->orderBy('e.arrangement', 'asc')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult();
        
        $medycyna = $em->createQueryBuilder()
            ->select('m')
            ->from('LsPageBundle:Page', 'm')
            ->where('m.id = 3')
            ->getQuery()
            ->getResult();
        

        return $this->render('LsCoreBundle:Default:index.html.twig', array(
            'slider' => $slider,
            'offers' => $offers,
            'news' => $news,
            'medycyna' => $medycyna[0]
        ));
    }

    public function adminAction() {
        $blocks = $this->container->getParameter('ls_core.admin.dashboard');
        $dashboard = new AdminDashboard();

        foreach ($blocks as $block) {
            $parent = new AdminBlock($block['label']);
            $dashboard->addBlock($parent);
            foreach ($block['items'] as $item) {
                $service = $this->container->get($item);
                $service->addToDashboard($parent);
            }
        }

        return $this->render('LsCoreBundle:Default:admin.html.twig', array(
            'dashboard' => $dashboard
        ));
    }
}
