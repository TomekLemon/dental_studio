<?php

namespace Ls\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GoogleRecaptchaType extends AbstractType {
    private $container;
    private $siteKey;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->siteKey = $this->container->get('cms_config')->get('google_recaptcha_site_key', '');

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'site_key' => $this->siteKey,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options) {
        $view->vars['site_key'] = $options['site_key'];
    }

    public function getParent() {
        return FormType::class;
    }

    public function getBlockPrefix() {
        return 'google_recaptcha';
    }
}
