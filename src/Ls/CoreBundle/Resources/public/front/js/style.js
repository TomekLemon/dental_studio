/**
 * Created by rafalglazar on 08/05/16.
 */

$(document).on('click', '.show-menu', function () {
    var container = $('#menu-mobile-container');

    if (!container.hasClass('visible')) {
        container.addClass('visible');
    }
});

$(document).on('click', '.hide-menu', function () {
    var container = $('#menu-mobile-container');

    if (container.hasClass('visible')) {
        container.removeClass('visible');
    }
});

$(document).on('click', '.show-person', function () {
    var container = $(this).parent().parent();

    if (!container.hasClass('visible')) {
        container.addClass('visible');
    }
});

$(document).on('click', '.hide-person', function () {
    var container = $(this).parent().parent();

    if (container.hasClass('visible')) {
        container.removeClass('visible');
    }
});

$(document).ready(function () {
    var main_slider = $('#main-slider');

    initMenuMobile();
    sliderHeight();

    $("a.fancybox").fancybox({
        'speedIn': 600,
        'speedOut': 200
    });

    if (main_slider.find('.slide').length > 0) {
        main_slider.cycle({
            fx: 'fade',
            slides: '.slide',
            timeout: 4000,
            speed: 750,
            pager: '.main-slider-pager',
            pagerTemplate: '<div class="pager-item"><div class="circle"></div></div>'
        });
    }
});

$(window).resize(function () {
    initMenuMobile();
    sliderHeight();
});

function initMenuMobile() {
    var ww = $(window).innerWidth();
    var wh = $(window).innerHeight();
    var cwh = 0;
    var mcth = $('#menu-mobile-container').find('.top').outerHeight();
    var mcmh = $('#menu-mobile').outerHeight();
    var mch = mcth + mcmh;

    $('#content-wrapper').css('height', '');

    if (ww < 660) {
        $('#menu-mobile-container').css('top', '-' + mch + 'px');
    } else {
        $('#menu-mobile-container').css('top', '0');

        if (ww < 1300) {
            cwh = $('#content-wrapper').outerHeight();

            if (cwh < wh) {
                cwh = wh;
            }
            if (cwh < mch) {
                cwh = mch;
            }
            $('#content-wrapper').css('height', cwh + 'px');
        }
    }
}

function sliderHeight() {
    var ww = $(window).innerWidth();
    var wh = $(window).innerHeight();
    var hh = $('header').outerHeight();
    var rh = wh - hh;
    var tt = 0;
    var spt = 0;
    var slider = $('#main-slider');


    if (slider.length > 0) {
        if (ww >= 1300) {
            if (rh < slider.find('.full').outerHeight() && rh >= 555) {
                tt = rh - (705 - 413);
                spt = rh - (705 - 565);
                slider.find('.full').css('height', rh + 'px');
                slider.find('.full').parent().css('height', rh + 'px');
                slider.find('.slide').find('.title').css('top', tt +'px');
                $('.main-slider-pager').css('top', spt +'px');
            }
            if (rh > slider.find('.full').outerHeight()) {
                if (rh <= 705) {
                    tt = rh - (705 - 413);
                    spt = rh - (705 - 565);
                    slider.find('.full').css('height', rh + 'px');
                    slider.find('.full').parent().css('height', rh + 'px');
                    slider.find('.slide').find('.title').css('top', tt + 'px');
                    $('.main-slider-pager').css('top', spt + 'px');
                } else {
                    slider.find('.full').css('height', '705px');
                    slider.find('.full').parent().css('height', '705px');
                    slider.find('.slide').find('.title').css('top', '413px');
                    $('.main-slider-pager').css('top', '565px');
                }
            }
        }
    }
}