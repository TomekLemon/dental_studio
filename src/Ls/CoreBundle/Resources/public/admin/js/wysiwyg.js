$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['/bundles/lscore/front/css/editor.css'],
        bodyClass: 'editor',
        toolbar: [
            {name: 'basicstyles', items: ['Source', '-', 'Bold', 'Italic']},
            {name: 'tools', items: ['Styles']},
            {name: 'paragraph', items: ['NumberedList', 'BulletedList', 'RemoveFormat']},
            {name: 'links', items: ['Link', 'Unlink', 'CreateDiv', 'ShowBlocks']},
            {name: 'insert', items: ['Image', '-', 'About']}
        ],
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Nagłówek', element: 'p', attributes: {'class': 'header'}},
            {name: 'Cytat', element: 'div', attributes: {'class': 'quote'}},
            {name: 'Wyróżnienie - ciemne', element: 'span', attributes: {'class': 'gray'}},
            {name: 'Wyróżnienie - różowe', element: 'span', attributes: {'class': 'pink'}},
        ],

        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscore/common/kcfinder-3.12/upload.php?type=flash'
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: 'auto'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
});