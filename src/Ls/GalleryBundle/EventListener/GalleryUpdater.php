<?php

namespace Ls\GalleryBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\GalleryBundle\Entity\Gallery;
use Ls\GalleryBundle\Entity\GalleryPhoto;

class GalleryUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoGenerate(false);
            }
            if (null === $entity->getArrangement() && $entity->getOnList()) {
                $qb = $em->createQueryBuilder();
                $query = $qb->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:Gallery', 'c')
                    ->where($qb->expr()->isNotNull('c.arrangement'))
                    ->andWhere('c.on_list = :on_list')
                    ->setParameter('on_list', true)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
            if (null !== $entity->getArrangement() && $entity->getOnList() == false) {
                $entity->setArrangement(null);
            }
        }

        if ($entity instanceof GalleryPhoto) {
            if (null === $entity->getArrangement()) {
                $gallery = $entity->getGallery();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:GalleryPhoto', 'c')
                    ->where('c.gallery = :gallery')
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                if (null !== $entity->getOldSlug()) {
                    $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsMenuBundle:MenuItem', 'c')
                        ->where('c.route LIKE :route')
                        ->andWhere('c.route_parameters LIKE :slug')
                        ->setParameter('route', 'ls_gallery_show')
                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                        ->getQuery();

                    $items = $query->getResult();

                    foreach ($items as $item) {
                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                        $em->persist($item);
                    }
                    $em->flush();

                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                } else {
                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                }
            }
            if (null !== $entity->getArrangement() && $entity->getOnList() == false) {
                $qb = $em->createQueryBuilder();
                $entities = $qb->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:Gallery', 'c')
                    ->where($qb->expr()->isNotNull('c.arrangement'))
                    ->andWhere('c.on_list = :on_list')
                    ->andWhere('c.id != :id')
                    ->setParameter('on_list', true)
                    ->setParameter('id', $entity->getId())
                    ->getQuery()
                    ->getResult();

                $arrangement = 1;
                foreach ($entities as $item) {
                    $item->setArrangement($arrangement);
                    $em->persist($item);
                    $arrangement++;
                }
                $em->flush();
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoGenerate(false);
            }
            if (null === $entity->getArrangement() && $entity->getOnList()) {
                $qb = $em->createQueryBuilder();
                $query = $qb->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:Gallery', 'c')
                    ->where($qb->expr()->isNotNull('c.arrangement'))
                    ->andWhere('c.on_list = :on_list')
                    ->setParameter('on_list', true)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
            if (null !== $entity->getArrangement() && $entity->getOnList() == false) {
                $entity->setArrangement(null);
            }
        }

        if ($entity instanceof GalleryPhoto) {
            if (null === $entity->getArrangement()) {
                $gallery = $entity->getGallery();
                $query = $em->createQueryBuilder()
                    ->select('COUNT(c.id)')
                    ->from('LsGalleryBundle:GalleryPhoto', 'c')
                    ->where('c.gallery = :gallery')
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $total = $query->getSingleScalarResult();
                $arrangement = $total + 1;
                $entity->setArrangement($arrangement);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsMenuBundle:MenuItem', 'c')
                    ->where('c.route LIKE :route')
                    ->andWhere('c.route_parameters LIKE :slug')
                    ->setParameter('route', 'ls_gallery_show')
                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                    ->getQuery();

                $items = $query->getResult();

                foreach ($items as $item) {
                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                    $em->persist($item);
                }
                $em->flush();

                $entity->setOldSlug($entity->getSlug());
                $em->persist($entity);
                $em->flush();
            }
            if (null !== $entity->getArrangement() && $entity->getOnList() == false) {
                $qb = $em->createQueryBuilder();
                $entities = $qb->select('c')
                    ->from('LsGalleryBundle:Gallery', 'c')
                    ->where($qb->expr()->isNotNull('c.arrangement'))
                    ->andWhere('c.on_list = :on_list')
                    ->andWhere('c.id != :id')
                    ->setParameter('on_list', true)
                    ->setParameter('id', $entity->getId())
                    ->getQuery()
                    ->getResult();

                $arrangement = 1;
                foreach ($entities as $item) {
                    $item->setArrangement($arrangement);
                    $em->persist($item);
                    $arrangement++;
                }
                $em->flush();
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof Gallery) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();

                $qb = $em->createQueryBuilder();
                $query = $qb->select('c')
                    ->from('LsGalleryBundle:Gallery', 'c')
                    ->where($qb->expr()->isNotNull('c.arrangement'))
                    ->andWhere('c.arrangement > :arrangement')
                    ->andWhere('c.on_list = :on_list')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('on_list', true)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsGalleryBundle:Gallery';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
        }

        if ($entity instanceof GalleryPhoto) {
            if (!isset($_SESSION['stopupdate'])) {
                $arrangement = $entity->getArrangement();
                $gallery = $entity->getGallery();

                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsGalleryBundle:GalleryPhoto', 'c')
                    ->where('c.arrangement > :arrangement')
                    ->andWhere('c.gallery = :gallery')
                    ->setParameter('arrangement', $arrangement)
                    ->setParameter('gallery', $gallery)
                    ->getQuery();

                $items = $query->getArrayResult();
                $ids = array();
                foreach ($items as $item) {
                    $ids[] = $item['id'];
                }
                $c = 0;
                if (isset($_SESSION['updateKolejnosc'])) {
                    $c = count($_SESSION['updateKolejnosc']);
                }
                $_SESSION['updateKolejnosc'][$c]['class'] = 'LsGalleryBundle:GalleryPhoto';
                $_SESSION['updateKolejnosc'][$c]['ids'] = $ids;
            }
            $entity->deletePhoto();
        }
    }
}