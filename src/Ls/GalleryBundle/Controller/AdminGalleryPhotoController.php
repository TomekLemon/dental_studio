<?php

namespace Ls\GalleryBundle\Controller;

use Ls\GalleryBundle\Entity\GalleryPhoto;
use Ls\GalleryBundle\Form\GalleryPhotoType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminGalleryPhotoController extends Controller {
    public function indexAction($galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
        $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('galleryId' => $galleryId)));

        return $this->render('LsGalleryBundle:AdminPhoto:index.html.twig', array(
            'gallery' => $gallery,
        ));
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/fileupload';
    }

    private function getMaxKolejnosc($gallery) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsGalleryBundle:GalleryPhoto', 'c')
            ->where('c.gallery = :gallery')
            ->setParameter('gallery', $gallery)
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function addManyAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $arrangement = $this->getMaxKolejnosc($gallery);

        $files = $request->request->get('files');

        foreach ($files as $filename) {
            $filename_array = explode('.', $filename);
            $ext = end($filename_array);
            $source = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $filename;

            $sFileName = uniqid('gallery-image-') . '.' . $ext;

            $entity = new GalleryPhoto();
            $entity->setGallery($gallery);
            $entity->setArrangement($arrangement);
            $entity->setPhoto($sFileName);
            $sSourceName = $entity->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

            if (!is_dir($entity->getUploadRootDir())) {
                $old_umask = umask(0);
                mkdir($entity->getUploadRootDir(), 0777);
                umask($old_umask);
            }

            copy($source, $sSourceName);

            $entity->createThumbs();

            $em->persist($entity);
            if (file_exists($source)) {
                @unlink($source);
            }
            $arrangement++;
        }
        $em->flush();

        return new Response('OK');
    }

    public function updateArrangementAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('LsGalleryBundle:GalleryPhoto');

        $items = json_decode($request->request->get('items'));
        $arrangement = 1;

        if (is_array($items)) {
            foreach ($items as $item) {
                $item_id = str_replace('item-', '', $item);
                $entity = $repository->find($item_id);
                if ($entity) {
                    $entity->setArrangement($arrangement);
                    $em->flush();

                    $arrangement++;
                }
            }
        }

        return new Response('OK');
    }

    public function editAction(Request $request, $galleryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = $em->getRepository('LsGalleryBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }
        $size = $entity->getThumbSize('detail');

        $form = $this->createForm(GalleryPhotoType::class, $entity, array(
            'action' => $this->generateUrl('ls_admin_gallery_photo_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Zapisz zmiany'));
        $form->add('submit_and_list', SubmitType::class, array('label' => 'Zapisz zmiany i zamknij'));

        $form->handleRequest($request);
        if ($form->isValid()) {
            if (null !== $entity->getFile()) {
                $entity->deletePhoto();

                $sFileName = uniqid('gallery-image-') . '.' . $entity->getFile()->guessExtension();
                $entity->setPhoto($sFileName);
                $entity->upload();
            }
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Aktualizacja zdjęcia zakończona sukcesem.');

            if ($form->get('submit')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));
            }
            if ($form->get('submit_and_list')->isClicked()) {
                return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
            }
        }
        if ($form->isSubmitted()) {
            $this->get('session')->getFlashBag()->add('error', 'Sprawdź pola formularza.');
        }

        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
        $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
        $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
        $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
        $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_photo_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));

        return $this->render('LsGalleryBundle:AdminPhoto:edit.html.twig', array(
            'form' => $form->createView(),
            'gallery' => $gallery,
            'entity' => $entity,
            'size' => $size,
        ));
    }

    public function deleteAction($galleryId, $id) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = $em->getRepository('LsGalleryBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add('success', 'Usunięcie zdjęcia zakończone sukcesem.');

        return new Response('OK');
    }

    public function kadrujAction(Request $request, $galleryId, $id) {
        $em = $this->getDoctrine()->getManager();
        $type = $request->get('type');

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = $em->getRepository('LsGalleryBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }

        if (null === $entity->getPhotoAbsolutePath()) {
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
        } else {
            $size = $entity->getThumbSize($type);
            $photo = $entity->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
            $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
            $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
            $breadcrumbs->addItem($entity->__toString(), $this->get('router')->generate('ls_admin_gallery_photo_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));
            $breadcrumbs->addItem('Kadrowanie', $this->get('router')->generate('ls_admin_gallery_photo_crop', array('galleryId' => $galleryId, 'id' => $entity->getId(), 'type' => $type)));

            return $this->render('LsGalleryBundle:AdminPhoto:kadruj.html.twig', array(
                'gallery' => $gallery,
                'entity' => $entity,
                'preview' => $preview,
                'thumb_conf' => $thumb_conf,
                'size' => $size,
                'aspect' => $thumb_ratio,
                'type' => $type,
            ));
        }
    }

    public function kadrujZapiszAction(Request $request, $galleryId, $id) {
        $em = $this->getDoctrine()->getManager();
        $type = $request->get('type');
        $x = $request->get('x');
        $y = $request->get('y');
        $x2 = $request->get('x2');
        $y2 = $request->get('y2');

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $entity = $em->getRepository('LsGalleryBundle:GalleryPhoto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GalleryPhoto entity.');
        }

        $entity->Thumb($x, $y, $x2, $y2, $type);

        $this->get('session')->getFlashBag()->add('success', 'Kadrowanie miniatury zakończone sukcesem.');

        return $this->redirect($this->generateUrl('ls_admin_gallery_photo_edit', array('galleryId' => $galleryId, 'id' => $entity->getId())));
    }

    public function batchAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $ids = $request->request->get('ids');
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = 'Czy na pewno chcesz ';
            switch ($action) {
                case 'delete':
                    $message .= 'usunąć ';
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'element?';
                    break;
                case 2:
                case 3:
                case 4:
                    $message .= 'elementy?';
                    break;
                default:
                    $message .= 'elementów?';
                    break;
            }

            $breadcrumbs = $this->get("white_october_breadcrumbs");
            $breadcrumbs->addItem('Dashboard', $this->get('router')->generate('ls_core_admin'));
            $breadcrumbs->addItem('Galerie', $this->get('router')->generate('ls_admin_gallery'));
            $breadcrumbs->addItem($gallery->__toString(), $this->get('router')->generate('ls_admin_gallery_edit', array('id' => $galleryId)));
            $breadcrumbs->addItem('Zdjęcia', $this->get('router')->generate('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
            $breadcrumbs->addItem('Potwierdzenie', $this->get('router')->generate('ls_admin_gallery_photo_batch', array('galleryId' => $galleryId)));

            return $this->render('LsGalleryBundle:AdminPhoto:batch.html.twig', array(
                'gallery' => $gallery,
                'message' => $message,
                'action' => $action,
                'ids' => implode(',', $ids),
            ));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
        }
    }

    public function batchExecuteAction(Request $request, $galleryId) {
        $em = $this->getDoctrine()->getManager();

        $gallery = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.id = :id')
            ->setParameter('id', $galleryId)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $gallery) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $ids = explode(',', $request->request->get('ids'));
        $action = $request->request->get('action');

        if (is_array($ids) && count($ids) > 0) {
            $elements = count($ids);
            $message = '';
            switch ($action) {
                case 'delete':
                    $message .= 'Usunięcie ';
                    $qb = $em->createQueryBuilder();
                    $query = $qb->select('e')
                        ->from('LsGalleryBundle:GalleryPhoto', 'e')
                        ->add('where', $qb->expr()->in('e.id', $ids))
                        ->getQuery();

                    $iterableResult = $query->iterate();
                    while (($row = $iterableResult->next()) !== false) {
                        $em->remove($row[0]);
                        $em->flush();
                    }
                    break;
            }
            $message .= $elements . ' ';
            switch ($elements) {
                case 1:
                    $message .= 'elementu ';
                    break;
                default:
                    $message .= 'elementów ';
                    break;
            }
            $message .= 'zakończone sukcesem ';

            $this->get('session')->getFlashBag()->add('success', $message);
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Nie wybrałeś żadnych elementów.');
            return $this->redirect($this->generateUrl('ls_admin_gallery_photo', array('galleryId' => $galleryId)));
        }
    }
}
