<?php

namespace Ls\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Gallery controller.
 *
 */
class FrontController extends Controller {

    /**
     * Lists all Gallery entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $entities = $qb->select('c')
            ->from('LsGalleryBundle:Gallery', 'c')
            ->where($qb->expr()->isNotNull('c.arrangement'))
            ->andWhere('(SELECT COUNT(p.id) FROM LsGalleryBundle:GalleryPhoto AS p WHERE p.gallery = c.id) > 0')
            ->andWhere('c.on_list = :on_list')
            ->setParameter('on_list', true)
            ->orderBy('c.arrangement', 'desc')
            ->getQuery()
            ->getResult();

        return $this->render('LsGalleryBundle:Front:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Gallery entity.
     *
     */
    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('g', 'p')
            ->from('LsGalleryBundle:Gallery', 'g')
            ->leftJoin('g.photos', 'p')
            ->where('g.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (null === $entity) {
            throw $this->createNotFoundException('Unable to find Gallery entity.');
        }

        $qb = $em->createQueryBuilder();
        $galleries = $qb->select('c')
            ->from('LsGalleryBundle:Gallery', 'c')
            ->where($qb->expr()->isNotNull('c.arrangement'))
            ->andWhere('(SELECT COUNT(p.id) FROM LsGalleryBundle:GalleryPhoto AS p WHERE p.gallery = c.id) > 0')
            ->andWhere('c.on_list = :on_list')
            ->setParameter('on_list', true)
            ->orderBy('c.arrangement', 'desc')
            ->getQuery()
            ->getResult();

        return $this->render('LsGalleryBundle:Front:show.html.twig', array(
            'galleries' => $galleries,
            'entity' => $entity,
        ));
    }
}
